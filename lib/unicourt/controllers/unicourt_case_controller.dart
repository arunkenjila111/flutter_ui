import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_ui/unicourt/constants/search_data.dart';

class UnicourtCaseController extends GetxController {
  //variables
  final caseId = ''.obs;
  final partiesToken = ''.obs;
  final attorneysToken = ''.obs;
  final relatedCasesToken = ''.obs;
  final docketEntriesToken = ''.obs;
  final documentsToken = ''.obs;
  final parties = [].obs;
  final attorneys = [].obs;
  final docketEntries = [].obs;
  final relatedCases = [].obs;
  final judges = [].obs;
  final partyAttorney = [].obs;

  //methods
  setCaseId(caseId) {
    caseId = caseId;
  } //setCaseId

  getTokens() async {
    partiesToken.value = '';
    attorneysToken.value = '';
    relatedCasesToken.value = '';
    docketEntriesToken.value = '';
    documentsToken.value = '';
    parties.value = [];
    var token = '42c2045dac93b964aada9f2da6786f885447a2d4';
    if (caseId.value != '') {
      try {
        var url = Uri.parse(
            'https://sapi.unicourt.com/rest/v1/case/$caseId/?token=$token');

        var response =
            await http.get(url, headers: {"Content-Type": "application/json"});
        var body = json.decode(response.body);
        var result = jsonEncode(body['data']);
        var tokenData = json.decode(result);
        var error = jsonEncode(body['error']);
        partiesToken.value = tokenData['parties_token'];

        attorneysToken.value = tokenData['attorneys_token'];
        relatedCasesToken.value = tokenData['related_cases_token'];
        docketEntriesToken.value = tokenData['docket_entries_token'];
        documentsToken.value = tokenData['documents_token'];
        // print('attorney token: $attorneysToken');
        // print('case id: $caseId');
      } catch (Exception) {
        print('Exception Occured in getting tokens : $Exception');
      }
    } else {
      print('Case Id is not available');
    }
  } //setTokens

  getParties() async {
    List myList = [];
    parties.value = [];
    var token = '42c2045dac93b964aada9f2da6786f885447a2d4';
    if (caseId.value != '') {
      try {
        var url = Uri.parse(
            'https://sapi.unicourt.com/rest/v1/case/$caseId/parties?token=$token&parties_token=$partiesToken&page_number=1');

        var response =
            await http.get(url, headers: {"Content-Type": "application/json"});
        var body = json.decode(response.body);
        var result = jsonEncode(body['data']);
        var partyData = json.decode(result);
        parties.value = partyData['parties'];
      } catch (Exception) {
        print('Exception Occured in getting parties : $Exception');
      }
    } else {
      print('Case Id is not available');
    }
  } //parties

  getAttorneys() async {
    List myList = [];
    attorneys.value = [];
    var token = '42c2045dac93b964aada9f2da6786f885447a2d4';
    if (caseId.value != '') {
      try {
        var url = Uri.parse(
            'https://sapi.unicourt.com/rest/v1/case/$caseId/attorneys?token=$token&attorneys_token=$attorneysToken&page_number=1');

        var response =
            await http.get(url, headers: {"Content-Type": "application/json"});
        var body = json.decode(response.body);
        var result = jsonEncode(body['data']);
        var partyData = json.decode(result);
        attorneys.value = partyData['attorneys'];
      } catch (Exception) {
        print('Exception Occured in getting attorney : $Exception');
      }
    } else {
      print('Case Id is not available');
    }
  } //getting Attorneys

  getDocketEntries() async {
    List myList = [];
    docketEntries.value = [];
    var token = '42c2045dac93b964aada9f2da6786f885447a2d4';
    if (caseId.value != '') {
      try {
        var url = Uri.parse(
            'https://sapi.unicourt.com/rest/v1/case/$caseId/docketEntries?token=$token&docket_entries_token=$docketEntriesToken');

        var response =
            await http.get(url, headers: {"Content-Type": "application/json"});
        var body = json.decode(response.body);
        var result = jsonEncode(body['data']);
        var partyData = json.decode(result);
        docketEntries.value = partyData['docket_entries'];
        // print('docket entry data');
        // docketEntries.forEach((element) {
        //   print(element['text']);
        // });
      } catch (Exception) {
        print('Exception Occured in getting attorney : $Exception');
      }
    } else {
      print('Case Id is not available');
    }
  } //getting Docket Entries

  getRelatedCases() async {
    List myList = [];
    relatedCases.value = [];
    var token = '42c2045dac93b964aada9f2da6786f885447a2d4';
    if (caseId.value != '') {
      try {
        var url = Uri.parse(
            'https://sapi.unicourt.com/rest/v1/case/$caseId/relatedCases?token=$token&related_cases_token =$relatedCasesToken&page_number=1');

        var response =
            await http.get(url, headers: {"Content-Type": "application/json"});
        var body = json.decode(response.body);
        var result = jsonEncode(body['message']);
        var partyData = json.decode(result);
        print('related cases data');
        print(partyData);

        // relatedCases.value = partyData['related_cases'];
        // print('related cases data');
        // docketEntries.forEach((element) {
        //   print(element);
        // });
      } catch (Exception) {
        print('Exception Occured in getting related cases : $Exception');
      }
    } else {
      print('Case Id is not available');
    }
  }

  getJudges() {}

  //get
} //end

import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_ui/unicourt/constants/search_data.dart';

class UnicourtSearchController extends GetxController {
  final token = '42c2045dac93b964aada9f2da6786f885447a2d4'.obs;
  //variables
  final searchTerm = ''.obs;
  final searchData = [].obs;
  final searchDataSingle = RxMap<dynamic, dynamic>({});
  final searchAttribute = 'Everything'.obs;
  final seachSortBy = "Filing Date".obs;
  final isEnterPressed = false.obs;
  final isError = false.obs;
  final errorMessage = ''.obs;
  final isDataReceived = false.obs;
  final pageNumber = 1.obs;
  final totalMatches = 0.obs;
  final pageMaximum = 1.obs;
  final pageMinimum = 1.obs;
  final canMoveNextScreen = true.obs;
  final canMovePreviousScreen = true.obs;

  //methods
  changeSearchTerm(newSearchTerm) {
    searchTerm.value = newSearchTerm;
  } //changeSearchTrem

  canMoveNextScreenMethod() {
    if (pageNumber.value * 10 < totalMatches.value)
      canMoveNextScreen.value = true;
    else
      canMoveNextScreen.value = false;
    print('next screen : ${canMoveNextScreen.value}');
    return canMoveNextScreen.value;
  } //canMoveNextScreen

  canMovePreviousScreenMethod() {
    if (pageNumber.value == 1)
      canMovePreviousScreen.value = false;
    else
      canMovePreviousScreen.value = true;
    print('Previous screen : ${canMovePreviousScreen.value}');
    print('page number:  ${pageNumber.value}');
    return canMovePreviousScreen.value;
  } //canMoveNextScreen

  moveToNextScreen() {
    if (pageNumber.value * 10 < totalMatches.value) {
      pageNumber.value++;
      getHttpSearchData();
    }
  } //moveToNextScreen

  moveToPreviousScreen() {
    if (pageNumber.value / 10 > 0) {
      pageNumber.value--;
      getHttpSearchData();
      if (pageNumber.value * 0 < totalMatches.value) {
        canMovePreviousScreen.value = true;
      }
    } else
      canMovePreviousScreen.value = false;
    print('previus screen : ${canMovePreviousScreen.value}');
  } //moveToPreviousScreen

  //gettingData
  getHttpSearchData() async {
    List myList = [];
    isDataReceived.value = false;
    searchData.value = [];
    isError.value = false;

    try {
      var url = Uri.parse(
          'https://sapi.unicourt.com/rest/v1/search/?token=42c2045dac93b964aada9f2da6786f885447a2d4');

      Map data = {
        "query": [
          {
            "search_terms": ['$searchTerm'],
            "attribute": '$searchAttribute',
          },
        ],
        "sort_by": "${seachSortBy.value}",
        "page": pageNumber.value,
      };
      //encode Map to JSON
      var body = json.encode(data);

      var response = await http.post(url,
          headers: {"Content-Type": "application/json"}, body: body);
      var mySearchData = json.decode(response.body);
      var result = jsonEncode(mySearchData['data']);
      var error = jsonEncode(mySearchData['error']);
      isError.value = error.toLowerCase() == 'true';
      errorMessage.value = jsonEncode(mySearchData['message']).toString();
      var total = (jsonEncode(mySearchData['data']['total_matches']));
      totalMatches.value = int.parse(jsonDecode(total.toString()));
      pageMaximum.value = totalMatches.value > (pageNumber.value * 10)
          ? (pageNumber.value * 10)
          : totalMatches.value;
      pageMinimum.value = ((pageNumber.value - 1) * 10) + 1;

      var cases = jsonDecode(result)['result'];
      for (var item in cases) {
        var myData = new Map();
        myData['case_number'] = item['case']['case_number'];
        myData['case_id'] = item['case']['case_id'];
        myData['case_name'] = item['case']['case_name'];
        myData['jurisdiction'] = item['case']['jurisdiction'];
        myData['case_type'] = item['case']['case_type'];
        myData['case_type_category'] = item['case']['case_type_category'];
        myData['case_type_group'] = item['case']['case_type_group'];
        myData['case_status_category'] = item['case']['case_status_category'];
        myData['case_status_name'] = item['case']['case_status_name'];
        myData['filing_date'] = item['case']['filing_date'];
        myData['courthouse'] = item['case']['courthouse'];
        myData['judges'] = 'Mark Hunt';
        myList.add(myData);
      }
      print(searchData);
      searchData.value = myList;

      isDataReceived.value = true;
      print('SearchData from search controller');
      print(searchData.length);
      print('Error Message : $errorMessage');
    } catch (Exception) {
      print('Exception Occured : $Exception');
    }
  } //searchData

} //controller

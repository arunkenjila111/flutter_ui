import 'package:flutter/material.dart';
import 'package:flutter_ui/unicourt/components/unicourt_recent_search.dart';
import 'package:flutter_ui/unicourt/components/unicourt_search_error_screen.dart';
import 'package:flutter_ui/unicourt/components/unicourt_search_no_result.dart';
import 'package:flutter_ui/unicourt/components/unicourt_search_option.dart';
import 'package:flutter_ui/unicourt/components/unicourt_search_result.dart';
import 'package:flutter_ui/unicourt/components/unicourt_search_bottom_buttons.dart';
import 'package:get/get.dart';
import 'package:flutter_ui/unicourt/controllers/unicourt_search_controller.dart';

class UnicourtSearchScreen extends StatefulWidget {
  UnicourtSearchScreen({Key key}) : super(key: key);

  @override
  _UnicourtSearchScreenState createState() => _UnicourtSearchScreenState();
}

class _UnicourtSearchScreenState extends State<UnicourtSearchScreen> {
  final UnicourtSearchController unicourtSearchController =
      Get.put(UnicourtSearchController());

  String chosenValue = "All";
  String textFieldHint = "Search by Everything";
  String searchTerm = '';
  var width;

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: buildAppBar(),
      body: Obx(
        () => Container(
          padding: EdgeInsets.all(12),
          child: unicourtSearchController.isEnterPressed.value == false
              ? Column(
                  children: [
                    UnicourtRecentSeach(),
                  ],
                )
              : unicourtSearchController.isError.value == true
                  ? Column(
                      children: [
                        UnicourtSearchErrorScreen(),
                      ],
                    )
                  : Column(
                      children: [
                        UnicourtSearchOption(),
                        UnicourtSearchResult(),
                        UnicourtSearchBottomButtons(),
                      ],
                    ),
        ),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      elevation: 2,
      backgroundColor: Colors.white,
      leading: IconButton(
          icon: Icon(
            Icons.arrow_back_outlined,
            color: Colors.black,
          ),
          onPressed: () {
            Get.toNamed('/');
          }),
      actions: [
        Container(
          width: width / 1.5,
          margin: EdgeInsets.symmetric(vertical: 8),
          padding: EdgeInsets.only(left: 8),
          decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.7),
            border: Border.all(
              color: Colors.white,
              width: 0.2,
            ),
          ),
          child: TextField(
            controller: TextEditingController()
              ..text = unicourtSearchController.searchTerm.value,
            textAlignVertical: TextAlignVertical.center,
            autofocus: false,
            onChanged: (newText) {
              unicourtSearchController.changeSearchTerm(newText);
              if (unicourtSearchController.searchTerm.value == '') {
                unicourtSearchController.isEnterPressed.value = false;
              }
            },
            onSubmitted: (myValue) {
              if (unicourtSearchController.searchTerm.value != '') {
                unicourtSearchController.isEnterPressed.value = true;

                unicourtSearchController.searchData.value = [];
                unicourtSearchController.pageNumber.value = 1;
                unicourtSearchController.totalMatches.value = 0;
                unicourtSearchController.pageMaximum.value = 1;
                unicourtSearchController.pageMinimum.value = 1;
                unicourtSearchController.canMoveNextScreen.value = true;
                unicourtSearchController.canMovePreviousScreen.value = true;

                unicourtSearchController.getHttpSearchData();
              } else {
                unicourtSearchController.isEnterPressed.value = false;
              }
            },
            decoration: InputDecoration(
              hintText: '$textFieldHint',
              border: InputBorder.none,
            ),
            style: TextStyle(
              fontSize: 18,
            ),
          ),
        ),
        buildDropDown(),
      ],
    );
  }

  Container buildDropDown() {
    return Container(
      padding: const EdgeInsets.all(0.0),
      child: DropdownButton<String>(
        value: chosenValue,
        //elevation: 5,
        style: TextStyle(color: Colors.black),

        items: <String>[
          'All',
          'Party',
          'Attorney',
          'Case No',
          'Case',
          'Judge',
          'Docket',
        ].map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(
              value,
            ),
          );
        }).toList(),
        onChanged: (String value) {
          setState(() {
            chosenValue = value;
            if (value == 'All') {
              textFieldHint = 'Search by Everything';
              unicourtSearchController.searchAttribute.value = 'Everything';
            } else if (value == 'Case') {
              textFieldHint = 'Search by Case Name';
              unicourtSearchController.searchAttribute.value = 'Case Name';
            } else if (value == 'Case No') {
              textFieldHint = 'Search by Case Number';
              unicourtSearchController.searchAttribute.value = 'Case Number';
            } else {
              textFieldHint = 'Search by $value';
              unicourtSearchController.searchAttribute.value = value;
            }
          });
        },
        underline: SizedBox(),
      ),
    );
  }
}

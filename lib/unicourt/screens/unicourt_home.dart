import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ui/unicourt/components/unicourt_recent_search.dart';
import 'package:flutter_ui/unicourt/components/unicourt_search.dart';
import 'package:flutter_ui/unicourt/constants/search_data.dart';
import 'package:http/http.dart' as http;

class UnicourtHome extends StatelessWidget {
  const UnicourtHome({Key key}) : super(key: key);

  void getHttpSearchData() async {
    try {
      var searchTerm = 'Hunt';
      var url = Uri.parse(
          'https://sapi.unicourt.com/rest/v1/search/?token=42c2045dac93b964aada9f2da6786f885447a2d4');

      Map data = {
        'query': [
          {
            "search_terms": ['$searchTerm']
          }
        ]
      };
      //encode Map to JSON
      var body = json.encode(data);

      var response = await http.post(url,
          headers: {"Content-Type": "application/json"}, body: body);
      // print("${response.statusCode}");
      // print("${response.body}");
      //

      var mySearchData = json.decode(response.body);
      // print(mySearchData['data']);

      var result = jsonEncode(mySearchData['data']);
      var cases = jsonDecode(result)['result'];
      for (var item in cases) {
        var myData = new Map();
        myData['case_name'] = item['case']['case_name'];
        myData['jurisdiction'] = item['case']['jurisdiction'];
        myData['case_type_group'] = item['case']['case_type_group'];
        myData['case_type'] = item['case']['case_type'];
        myData['case_number'] = item['case']['case_number'];
        myData['filing_date'] = item['case']['filing_date'];
        print(item['case']['case_name']);
      }
    } catch (Exception) {
      print('Exception Occured : $Exception');
    }
  } //httpSearchData

  void getHttpCaseData() async {
    print('getting http data');
    final response = await http.get(
      Uri.parse(
          "https://sapi.unicourt.com/rest/v1/case/GBFR6IZVIY3VAHAZIJNGVCTENBKRS0908/?token=42c2045dac93b964aada9f2da6786f885447a2d4"),
    );
    var decodedData = jsonDecode(response.body);
    print(decodedData);
  } //httpCaseData

  Future<String> loadJsonFromAssets() async {
    return await rootBundle.loadString('json/search_data.json');
  } //loadingJson

  Future loadParsedJson() async {
    String jsonString = await loadJsonFromAssets();
    final jsonResponse = json.decode(jsonString);
    print('json');
    print(jsonResponse);
    for (var item in jsonResponse) {
      // print(item['case']['case_type']);
    }
  } //parsedJson

  @override
  Widget build(BuildContext context) {
    // loadParsedJson();
    // getMyData();
    //getHttpCaseData();
    getHttpSearchData();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Unicourt',
          style: TextStyle(
            color: Colors.blue[700],
          ),
        ),
        backgroundColor: Colors.white,
        actions: [
          Icon(
            Icons.menu_outlined,
            color: Colors.grey,
          ),
          SizedBox(
            width: 10,
          ),
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(8),
        child: Column(
          children: [
            UnicourtSearch(),
            SizedBox(
              height: 15,
            ),
            UnicourtRecentSeach(),
          ],
        ),
      ),
    );
  }

  void getMyData() async {
    var result = jsonEncode(searchData[0]['data']);
    var cases = jsonDecode(result)['result'];
    for (var item in cases) {
      print('cases');
      print(item['case']);
    }
  } //endGetData
} //class

import 'package:flutter/material.dart';
import 'package:flutter_ui/unicourt/components/unicourt_search_tab.dart';
import 'package:flutter_ui/unicourt/controllers/unicourt_case_controller.dart';
import 'package:flutter_ui/unicourt/controllers/unicourt_search_controller.dart';
import 'package:get/get.dart';

class UnicourtSearchDetailScreen extends StatefulWidget {
  UnicourtSearchDetailScreen({Key key}) : super(key: key);

  @override
  _UnicourtSearchDetailScreenState createState() =>
      _UnicourtSearchDetailScreenState();
}

class _UnicourtSearchDetailScreenState
    extends State<UnicourtSearchDetailScreen> {
  final UnicourtSearchController unicourtSearchController =
      Get.put(UnicourtSearchController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: Container(
        padding: EdgeInsets.all(12),
        child: Column(
          children: [
            Container(
              color: Colors.white,
              child: Center(
                child: Obx(
                  () => Text.rich(
                    TextSpan(
                      text:
                          '${unicourtSearchController.searchDataSingle['case_name']}',
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                      children: [
                        TextSpan(
                          text: '   Updated 11 minutes ago',
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            UnicourtSeachTab(),
          ],
        ),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      elevation: 2,
      backgroundColor: Colors.white,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_outlined,
          color: Colors.black,
        ),
        onPressed: () {
          Get.toNamed('search');
        },
      ),
      title: Text(
        'Search Result',
        style: TextStyle(
          color: Colors.black,
        ),
      ),
      actions: [
        IconButton(
          icon: Icon(
            Icons.save_outlined,
            color: Colors.green[700],
          ),
          onPressed: () {},
        ),
        IconButton(
          icon: Icon(
            Icons.update_outlined,
            color: Colors.green[700],
          ),
          onPressed: () {},
        ),
        PopupMenuButton<String>(
          icon: Icon(
            Icons.more_vert_outlined,
            color: Colors.green[700],
          ),
          onSelected: handleClick,
          itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
            const PopupMenuItem<String>(
              value: 'Value1',
              child: ListTile(
                leading: Icon(
                  Icons.share_outlined,
                  color: Colors.green,
                ),
                title: Text('Share'),
              ),
            ),
            const PopupMenuItem<String>(
              value: 'Value2',
              child: ListTile(
                leading: Icon(
                  Icons.flag_outlined,
                  color: Colors.green,
                ),
                title: Text('Track'),
              ),
            ),
          ],
        ),
      ],
    );
  }

  void handleClick(String value) {}
}

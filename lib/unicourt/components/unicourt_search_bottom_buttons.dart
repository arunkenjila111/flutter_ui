import 'package:flutter/material.dart';
import 'package:flutter_ui/unicourt/controllers/unicourt_search_controller.dart';
import 'package:get/get.dart';

class UnicourtSearchBottomButtons extends StatefulWidget {
  const UnicourtSearchBottomButtons({Key key}) : super(key: key);

  @override
  _UnicourtSearchBottomButtonsState createState() =>
      _UnicourtSearchBottomButtonsState();
}

class _UnicourtSearchBottomButtonsState
    extends State<UnicourtSearchBottomButtons> {
  final UnicourtSearchController unicourtSearchController =
      Get.put(UnicourtSearchController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        child: unicourtSearchController.searchData.length == 0
            ? null
            : Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MaterialButton(
                    elevation: 2,
                    disabledColor: Colors.red,
                    color:
                        unicourtSearchController.canMovePreviousScreenMethod()
                            ? Colors.white
                            : Colors.grey,
                    onPressed: () {
                      unicourtSearchController.canMovePreviousScreenMethod()
                          ? unicourtSearchController.moveToPreviousScreen()
                          : () {};
                    },
                    child: Icon(
                      Icons.navigate_before_outlined,
                    ),
                    shape: CircleBorder(),
                  ),
                  MaterialButton(
                    elevation: 2,
                    disabledColor: Colors.red,
                    color: unicourtSearchController.canMoveNextScreenMethod()
                        ? Colors.white
                        : Colors.grey,
                    onPressed: () {
                      unicourtSearchController.canMoveNextScreenMethod()
                          ? unicourtSearchController.moveToNextScreen()
                          : null;
                    },
                    child: Icon(
                      Icons.navigate_next_outlined,
                    ),
                    shape: CircleBorder(),
                  ),
                ],
              ),
      ),
    );
  }
}

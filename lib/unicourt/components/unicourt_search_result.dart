import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_ui/unicourt/components/unicourt_search_no_result.dart';
import 'package:flutter_ui/unicourt/constants/search_data.dart';
import 'package:flutter_ui/unicourt/controllers/unicourt_case_controller.dart';
import 'package:flutter_ui/unicourt/controllers/unicourt_search_controller.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class UnicourtSearchResult extends StatefulWidget {
  UnicourtSearchResult({Key key}) : super(key: key);

  @override
  _UnicourtSearchResultState createState() => _UnicourtSearchResultState();
}

class _UnicourtSearchResultState extends State<UnicourtSearchResult> {
  final UnicourtSearchController unicourtSearchController =
      Get.put(UnicourtSearchController());
  final UnicourtCaseController unicourtCaseController =
      Get.put(UnicourtCaseController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Expanded(
        flex: 12,
        child: unicourtSearchController.isDataReceived.value == false
            ? Align(
                child: Container(
                  child: CircularProgressIndicator(),
                ),
              )
            : unicourtSearchController.searchData.length == 0
                ? UnicourtSearchNoResult()
                : Container(
                    child: Obx(
                      () => ListView.builder(
                        itemCount: unicourtSearchController.searchData.length,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              unicourtSearchController.searchDataSingle.value =
                                  unicourtSearchController.searchData[index];

                              Future.delayed(
                                Duration(milliseconds: 200),
                                () => Get.toNamed('searchDetail'),
                              );
                            },
                            child: Container(
                              padding: EdgeInsets.all(4),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    '${unicourtSearchController.searchData[index]['case_name']}',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'roboto',
                                      fontSize: 14,
                                      color: Colors.blue[800],
                                      letterSpacing: 1,
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          '${unicourtSearchController.searchData[index]['jurisdiction']}',
                                        ),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Expanded(
                                        child: Align(
                                          alignment: Alignment.centerRight,
                                          child: Text(
                                            '${unicourtSearchController.searchData[index]['case_type_group']}',
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 5),
                                  Divider(
                                    color: Colors.grey,
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
      ),
    );
  } //build

} //class

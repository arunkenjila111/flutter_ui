import 'package:flutter/material.dart';
import 'package:flutter_ui/unicourt/components/unicourt_docket_case_details.dart';
import 'package:flutter_ui/unicourt/components/unicourt_docket_entries.dart';

class DocketTab extends StatelessWidget {
  const DocketTab({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            UnicourtDocketEntries(),
          ],
        ),
      ),
    );
  } //build
} //class

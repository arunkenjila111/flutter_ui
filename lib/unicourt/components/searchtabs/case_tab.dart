import 'package:flutter/material.dart';
import 'package:flutter_ui/unicourt/components/unicourt_docket_case_details.dart';
import 'package:flutter_ui/unicourt/constants/search_data.dart';
import 'package:flutter_ui/unicourt/controllers/unicourt_search_controller.dart';
import 'package:get/get.dart';

class CaseTab extends StatefulWidget {
  const CaseTab({Key key}) : super(key: key);

  @override
  _CaseTabState createState() => _CaseTabState();
}

class _CaseTabState extends State<CaseTab> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            UnicourtDocketCaseDetails(),
          ],
        ),
      ),
    );
  }
} //class

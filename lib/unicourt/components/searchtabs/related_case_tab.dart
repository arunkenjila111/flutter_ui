import 'package:flutter/material.dart';
import 'package:flutter_ui/unicourt/controllers/unicourt_case_controller.dart';
import 'package:get/get.dart';

class RelatedCaseTab extends StatefulWidget {
  const RelatedCaseTab({Key key}) : super(key: key);

  @override
  _RelatedCaseTabState createState() => _RelatedCaseTabState();
}

class _RelatedCaseTabState extends State<RelatedCaseTab> {
  final UnicourtCaseController unicourtCaseController =
      Get.put(UnicourtCaseController());

  gettingRelatedCasesData() async {
    await unicourtCaseController.getTokens();
    await unicourtCaseController.getRelatedCases();
  }

  @override
  void initState() {
    // TODO: implement initState
    gettingRelatedCasesData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text('Related Cases...'),
        ],
      ),
    );
  }
}

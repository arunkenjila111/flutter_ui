import 'package:flutter/material.dart';
import 'package:flutter_ui/unicourt/components/unicourt_attorneys_list.dart';
import 'package:flutter_ui/unicourt/controllers/unicourt_case_controller.dart';
import 'package:get/get.dart';

class AttorneysTab extends StatefulWidget {
  const AttorneysTab({Key key}) : super(key: key);

  @override
  _AttorneysTabState createState() => _AttorneysTabState();
}

class _AttorneysTabState extends State<AttorneysTab> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            UnicourtAttorneysList(),
          ],
        ),
      ),
    );
  }
}

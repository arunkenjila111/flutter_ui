import 'package:flutter/material.dart';
import 'package:flutter_ui/unicourt/components/unicourt_available_document.dart';
import 'package:flutter_ui/unicourt/components/unicourt_download_document.dart';

class DocumentsTab extends StatelessWidget {
  const DocumentsTab({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            UnicourtDownloadDocument(),
            SizedBox(
              height: 20,
            ),
            UnicourtAvailableDocument(),
          ],
        ),
      ),
    );
  }
}

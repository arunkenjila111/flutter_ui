import 'package:flutter/material.dart';

class UnicourtRecentSeach extends StatelessWidget {
  const UnicourtRecentSeach({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 10,
      child: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.white,
        child: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Text(
                'Recent Searches',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
            ),
            ListTile(
              leading: Text(
                'Arun Kenjila',
                style: TextStyle(
                  fontSize: 17,
                ),
              ),
              trailing: TextButton(
                onPressed: () {},
                child: Text(
                  'Search',
                  style: TextStyle(
                    color: Colors.green[700],
                  ),
                ),
              ),
            ),
            ListTile(
              leading: Text(
                'John',
                style: TextStyle(
                  fontSize: 17,
                ),
              ),
              trailing: TextButton(
                onPressed: () {},
                child: Text(
                  'Search',
                  style: TextStyle(
                    color: Colors.green[700],
                  ),
                ),
              ),
            ),
            ListTile(
              leading: Text(
                'mark',
                style: TextStyle(
                  fontSize: 17,
                ),
              ),
              trailing: TextButton(
                onPressed: () {},
                child: Text(
                  'Search',
                  style: TextStyle(
                    color: Colors.green[700],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

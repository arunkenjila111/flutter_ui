import 'package:flutter/material.dart';
import 'package:flutter_ui/unicourt/controllers/unicourt_search_controller.dart';
import 'package:get/get.dart';

class UnicourtSearchErrorScreen extends StatefulWidget {
  UnicourtSearchErrorScreen({Key key}) : super(key: key);

  @override
  _UnicourtSearchErrorScreenState createState() =>
      _UnicourtSearchErrorScreenState();
}

class _UnicourtSearchErrorScreenState extends State<UnicourtSearchErrorScreen> {
  final UnicourtSearchController unicourtSearchController =
      Get.put(UnicourtSearchController());
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Center(
            child: Container(
              height: 350,
              width: 350,
              child: Image.asset('assets/error.png'),
            ),
          ),
          Obx(
            () => Text(
              '${unicourtSearchController.errorMessage}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_ui/unicourt/controllers/unicourt_case_controller.dart';
import 'package:flutter_ui/unicourt/controllers/unicourt_search_controller.dart';
import 'package:get/get.dart';

class UnicourtDocketCaseDetails extends StatefulWidget {
  const UnicourtDocketCaseDetails({Key key}) : super(key: key);

  @override
  _UnicourtDocketCaseDetailsState createState() =>
      _UnicourtDocketCaseDetailsState();
}

class _UnicourtDocketCaseDetailsState extends State<UnicourtDocketCaseDetails> {
  final UnicourtSearchController unicourtSearchController =
      Get.put(UnicourtSearchController());
  final UnicourtCaseController unicourtCaseController =
      Get.put(UnicourtCaseController());
  @override
  Widget build(BuildContext context) {
    unicourtCaseController.caseId.value =
        unicourtSearchController.searchDataSingle['case_id'];
    unicourtCaseController.getTokens();
    unicourtCaseController.getParties();

    return Container(
      width: double.infinity,
      child: Table(
        defaultColumnWidth: FixedColumnWidth(80.0),
        columnWidths: {
          0: FlexColumnWidth(1),
          1: FlexColumnWidth(2),
        },
        children: [
          TableRow(
            children: [
              CaseTitle(
                caseTitle: 'Case Number',
              ),
              CaseValue(
                caseValue:
                    '${unicourtSearchController.searchDataSingle['case_number']}',
              )
            ],
          ),
          buildEmptyTableRow(),
          TableRow(
            children: [
              CaseTitle(
                caseTitle: 'Case Status',
              ),
              CaseValue(
                caseValue:
                    '${unicourtSearchController.searchDataSingle['case_status_category']} - ${unicourtSearchController.searchDataSingle['case_status_name']}',
              )
            ],
          ),
          buildEmptyTableRow(),
          TableRow(
            children: [
              CaseTitle(
                caseTitle: 'Filing Date',
              ),
              CaseValue(
                caseValue:
                    '${unicourtSearchController.searchDataSingle['filing_date']}',
              )
            ],
          ),
          buildEmptyTableRow(),
          TableRow(
            children: [
              CaseTitle(
                caseTitle: 'Judges',
              ),
              CaseValue(
                caseValue:
                    '${unicourtSearchController.searchDataSingle['judges']}',
              )
            ],
          ),
          buildEmptyTableRow(),
          TableRow(
            children: [
              CaseTitle(
                caseTitle: 'Jurisdiction',
              ),
              CaseValue(
                caseValue:
                    '${unicourtSearchController.searchDataSingle['jurisdiction']}',
              )
            ],
          ),
          buildEmptyTableRow(),
          TableRow(
            children: [
              CaseTitle(
                caseTitle: 'Courthouse',
              ),
              CaseValue(
                caseValue:
                    '${unicourtSearchController.searchDataSingle['courthouse']}',
              )
            ],
          ),
          buildEmptyTableRow(),
          TableRow(
            children: [
              CaseTitle(
                caseTitle: 'Case Type',
              ),
              CaseValue(
                caseValue:
                    '${unicourtSearchController.searchDataSingle['case_type']} - ${unicourtSearchController.searchDataSingle['case_type_category']} - ${unicourtSearchController.searchDataSingle['case_type_group']}',
              )
            ],
          ),
          buildEmptyTableRow(),
        ],
      ),
    );
  }

  TableRow buildEmptyTableRow() {
    return TableRow(
      children: [
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
} //class

class CaseValue extends StatelessWidget {
  const CaseValue({
    Key key,
    this.caseValue,
  }) : super(key: key);

  final String caseValue;

  @override
  Widget build(BuildContext context) {
    return Text(
      '$caseValue',
      style: TextStyle(
        fontSize: 15,
      ),
    );
  }
}

class CaseTitle extends StatelessWidget {
  const CaseTitle({
    Key key,
    this.caseTitle,
  }) : super(key: key);
  final String caseTitle;

  @override
  Widget build(BuildContext context) {
    return Text(
      '$caseTitle:',
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 16,
      ),
    );
  }
}

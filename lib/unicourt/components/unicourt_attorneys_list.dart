import 'package:flutter/material.dart';
import 'package:flutter_ui/unicourt/controllers/unicourt_case_controller.dart';
import 'package:get/get.dart';

class UnicourtAttorneysList extends StatefulWidget {
  const UnicourtAttorneysList({Key key}) : super(key: key);

  @override
  _UnicourtAttorneysListState createState() => _UnicourtAttorneysListState();
}

class _UnicourtAttorneysListState extends State<UnicourtAttorneysList> {
  final UnicourtCaseController unicourtCaseController =
      Get.put(UnicourtCaseController());

  List<dynamic> structuredAttorneyList = [];

  gettingAttorneyList() {
    print('getting attorney');
    unicourtCaseController.attorneys.forEach((element) {
      var item;
      bool addItem = false;
      unicourtCaseController.partyAttorney.forEach((innerElement) {
        if (element['attorney_id'] == innerElement['attorneyId']) {
          item = {
            'attorneyName': element['fullname'],
            'analytics': '-',
            'lawFirm': element['firm'],
            'parties': innerElement['fullName'],
          };
          addItem = true;
        }
      });
      if (addItem == true) structuredAttorneyList.add(item);
    });
  } //attorneyList

  @override
  Widget build(BuildContext context) {
    gettingAttorneyList();
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10,
          ),
          Text.rich(
            TextSpan(
                text: '${structuredAttorneyList.length}',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
                children: [
                  TextSpan(
                    text: ' Attorneys in this case',
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ]),
          ),
          SizedBox(
            height: 10,
          ),
          Divider(
            color: Colors.grey,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Table(
              defaultColumnWidth: FixedColumnWidth(140.0),
              children: [
                buildTableHeader(),
                TableRow(
                  children: [
                    Divider(
                      color: Colors.grey,
                    ),
                    Divider(
                      color: Colors.grey,
                    ),
                    Divider(
                      color: Colors.grey,
                    ),
                    Divider(
                      color: Colors.grey,
                    ),
                  ],
                ),

                // buildTableRow(),
                buildEmptyTableRow(),
                for (var item in structuredAttorneyList)
                  TableRow(
                    children: [
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        child: Text(
                          item['attorneyName'],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        child: Text(
                          item['analytics'],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        child: Text(
                          item['lawFirm'] != null ? 'law firm' : '',
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        child: Text(
                          item['parties'],
                        ),
                      ),
                    ],
                  ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  TableRow buildTableRow() {
    return TableRow(
      children: [
        Text(
          'Bank Of America NA',
          style: TextStyle(
            fontSize: 13,
          ),
        ),
        Text(
          'Plaintiff',
          style: TextStyle(
            fontSize: 13,
          ),
        ),
        Text(
          'Unrepresented',
          style: TextStyle(
            fontSize: 13,
          ),
        ),
        Text(
          '-',
          style: TextStyle(
            fontSize: 13,
          ),
        ),
      ],
    );
  }

  TableRow buildTableHeader() {
    return TableRow(
      children: [
        Text(
          'Attorney Name..',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey[600],
            fontSize: 15,
          ),
        ),
        Text(
          'Analytics',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey[600],
            fontSize: 15,
          ),
        ),
        Text(
          'Law Firm',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey[600],
            fontSize: 15,
          ),
        ),
        Text(
          'Parties',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey[600],
            fontSize: 15,
          ),
        ),
      ],
    );
  }

  TableRow buildEmptyTableRow() {
    return TableRow(
      children: [
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
} //class

import 'package:flutter/material.dart';

class UnicourtAvailableDocument extends StatelessWidget {
  const UnicourtAvailableDocument({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Available Documents',
            style: TextStyle(
              fontSize: 18,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Divider(
            color: Colors.grey,
          ),
          SizedBox(
            height: 5,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Table(
              defaultColumnWidth: FixedColumnWidth(140.0),
              children: [
                buildTableHeader(),
                TableRow(children: [
                  Divider(
                    color: Colors.grey,
                  ),
                  Divider(
                    color: Colors.grey,
                  ),
                  Divider(
                    color: Colors.grey,
                  ),
                ]),
                buildTableRow(),
                buildEmptyTableRow(),
                buildTableRow(),
                buildEmptyTableRow(),
                buildTableRow(),
                buildEmptyTableRow(),
                buildTableRow(),
                buildEmptyTableRow(),
                buildTableRow(),
                buildEmptyTableRow(),
                buildTableRow(),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Align(
            alignment: Alignment.center,
            child: TextButton(
              child: Text(
                'Download',
                style: TextStyle(
                  color: Colors.grey[700],
                ),
              ),
              onPressed: () {},
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.grey),
              ),
            ),
          ),
        ],
      ),
    );
  }

  TableRow buildTableRow() {
    return TableRow(
      children: [
        Text(
          'CIVIL COVER SHEET',
          style: TextStyle(
            fontSize: 13,
          ),
        ),
        Text(
          '04/26/2021: CIVIL COVER SHEET',
          style: TextStyle(
            fontSize: 13,
          ),
        ),
        Text(
          '-',
          style: TextStyle(
            fontSize: 13,
          ),
        ),
      ],
    );
  }

  TableRow buildTableHeader() {
    return TableRow(
      children: [
        Text(
          'Document',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey[600],
            fontSize: 15,
          ),
        ),
        Text(
          'Description',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey[600],
            fontSize: 15,
          ),
        ),
        Text(
          'Downloaded',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey[600],
            fontSize: 15,
          ),
        ),
      ],
    );
  }

  TableRow buildEmptyTableRow() {
    return TableRow(
      children: [
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
} //class

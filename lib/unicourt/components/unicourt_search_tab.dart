import 'package:flutter/material.dart';
import 'package:flutter_ui/furnitureApp/components/ProductList.dart';
import 'package:flutter_ui/unicourt/components/searchtabs/attorneys_tab.dart';
import 'package:flutter_ui/unicourt/components/searchtabs/case_tab.dart';
import 'package:flutter_ui/unicourt/components/searchtabs/docket_history_tab.dart';
import 'package:flutter_ui/unicourt/components/searchtabs/docket_tab.dart';
import 'package:flutter_ui/unicourt/components/searchtabs/documents_tab.dart';
import 'package:flutter_ui/unicourt/components/searchtabs/parties_tab.dart';
import 'package:flutter_ui/unicourt/components/searchtabs/related_case_tab.dart';

class UnicourtSeachTab extends StatefulWidget {
  UnicourtSeachTab({Key key}) : super(key: key);

  @override
  _UnicourtSeachTabState createState() => _UnicourtSeachTabState();
}

class _UnicourtSeachTabState extends State<UnicourtSeachTab>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    tabController = TabController(length: 7, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
        flex: 9,
        child: Column(
          children: [
            TabBar(
              labelColor: Colors.grey[700],
              indicatorColor: Colors.green[700],
              unselectedLabelColor: Colors.blue[700],
              isScrollable: true,
              controller: tabController,
              tabs: [
                Tab(
                  child: MyCustomTab(
                    icon: Icon(
                      Icons.source_outlined,
                    ),
                    title: 'CASE',
                  ),
                ),
                Tab(
                  child: MyCustomTab(
                    icon: Icon(
                      Icons.folder_outlined,
                    ),
                    title: 'DOCKET',
                  ),
                ),
                Tab(
                  child: MyCustomTab(
                    icon: Icon(
                      Icons.folder_outlined,
                    ),
                    title: 'DOCUMENTS',
                  ),
                ),
                Tab(
                  child: MyCustomTab(
                    icon: Icon(Icons.people_outlined),
                    title: 'PARTIES',
                  ),
                ),
                Tab(
                  child: MyCustomTab(
                    icon: Icon(
                      Icons.person_outline_outlined,
                    ),
                    title: 'ATTORNEYS',
                  ),
                ),
                Tab(
                  child: MyCustomTab(
                    icon: Icon(
                      Icons.history_outlined,
                    ),
                    title: 'DOCKET HISTORY',
                  ),
                ),
                Tab(
                  child: MyCustomTab(
                    icon: Icon(
                      Icons.link_outlined,
                    ),
                    title: 'RELATED CASES',
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Expanded(
              child: TabBarView(
                controller: tabController,
                children: [
                  CaseTab(),
                  DocketTab(),
                  DocumentsTab(),
                  PartiesTab(),
                  AttorneysTab(),
                  DocketHistoryTab(),
                  RelatedCaseTab(),
                ],
              ),
            ),
          ],
        ));
  }
}

class MyCustomTab extends StatelessWidget {
  const MyCustomTab({
    Key key,
    this.icon,
    this.title,
  }) : super(key: key);

  final Icon icon;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        icon,
        SizedBox(
          width: 5,
        ),
        Text('$title'),
      ],
    );
  }
}

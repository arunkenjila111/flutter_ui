import 'package:flutter/material.dart';
import 'package:flutter_ui/unicourt/controllers/unicourt_case_controller.dart';
import 'package:get/get.dart';

class UnicourtDocketEntries extends StatefulWidget {
  const UnicourtDocketEntries({Key key}) : super(key: key);

  @override
  _UnicourtDocketEntriesState createState() => _UnicourtDocketEntriesState();
}

class _UnicourtDocketEntriesState extends State<UnicourtDocketEntries> {
  final UnicourtCaseController unicourtCaseController =
      Get.put(UnicourtCaseController());

  getStructuredDocketEntryList() async {
    await unicourtCaseController.getTokens();
    await unicourtCaseController.getDocketEntries();
  } //get docket entries

  @override
  Widget build(BuildContext context) {
    getStructuredDocketEntryList();
    return Obx(
      () => Container(
        width: double.infinity,
        child: unicourtCaseController.docketEntries.length == 0
            ? Container(
                child: Align(
                  child: Container(
                    width: 50,
                    height: 50,
                    child: CircularProgressIndicator(),
                  ),
                ),
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Docket Entries',
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Divider(
                    color: Colors.grey,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Table(
                      defaultColumnWidth: FixedColumnWidth(150.0),
                      // columnWidths: {
                      //   0: FlexColumnWidth(1),
                      //   1: FlexColumnWidth(4),
                      //   2: FlexColumnWidth(4),
                      //   3: FlexColumnWidth(4),
                      // },
                      children: [
                        buildTableHeader(),
                        buildLineTableRow(),
                        for (var item in unicourtCaseController.docketEntries)
                          TableRow(
                            children: [
                              Container(
                                margin: EdgeInsets.symmetric(vertical: 10),
                                child: Text(
                                  'docket',
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(vertical: 10),
                                child: Text(
                                  item['date'],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(vertical: 10),
                                child: Text(
                                  item['text'],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(vertical: 10),
                                child: Text(
                                  '',
                                ),
                              ),
                            ],
                          )
                      ],
                    ),
                  ),
                ],
              ),
      ),
    );
  }

  TableRow buildLineTableRow() {
    return TableRow(
      children: [
        Divider(
          color: Colors.grey,
        ),
        Divider(
          color: Colors.grey,
        ),
        Divider(
          color: Colors.grey,
        ),
        Divider(
          color: Colors.grey,
        ),
      ],
    );
  }

  TableRow buildTableRow() {
    return TableRow(
      children: [
        Text(
          'docket',
        ),
        Text(
          '04/26/2021',
        ),
        Text(
          ' ',
        ),
        Text(
          'NOTICE OF CONFIDENTIAL INFO RULE 2-420; Additional Info: Plaintiff: Bank Of America NA',
        ),
      ],
    );
  }

  TableRow buildTableHeader() {
    return TableRow(
      children: [
        Text(
          'Type',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey,
            fontSize: 15,
          ),
        ),
        Text(
          'Date',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey,
            fontSize: 15,
          ),
        ),
        Text(
          'Docket Entries',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey,
            fontSize: 15,
          ),
        ),
        Text(
          'Document',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey,
            fontSize: 15,
          ),
        ),
      ],
    );
  }

  TableRow buildEmptyTableRow() {
    return TableRow(
      children: [
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
} //class

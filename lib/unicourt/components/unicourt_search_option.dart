import 'package:flutter/material.dart';
import 'package:flutter_ui/unicourt/controllers/unicourt_search_controller.dart';
import 'package:get/get.dart';

class UnicourtSearchOption extends StatefulWidget {
  UnicourtSearchOption({Key key}) : super(key: key);

  @override
  _UnicourtSearchOptionState createState() => _UnicourtSearchOptionState();
}

class _UnicourtSearchOptionState extends State<UnicourtSearchOption> {
  final UnicourtSearchController unicourtSearchController =
      Get.put(UnicourtSearchController());
  String chosenValue = 'Date(desc)';

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Expanded(
        flex: 1,
        child: unicourtSearchController.searchData.length == 0
            ? Container(
                color: Colors.white,
              )
            : Container(
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Obx(
                        () => Text.rich(
                          TextSpan(
                            text:
                                'View ${unicourtSearchController.pageMinimum.value}- ${unicourtSearchController.pageMaximum.value} of',
                            children: [
                              TextSpan(
                                text:
                                    ' ${unicourtSearchController.totalMatches}',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' results',
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    buildFilter(),
                  ],
                ),
              ),
      ),
    );
  }

  Container buildFilter() {
    return Container(
      padding: const EdgeInsets.all(0.0),
      child: Row(
        children: [
          Container(
            child: Row(
              children: [
                Text(
                  'Sort By',
                  style: TextStyle(
                    color: Colors.green[700],
                  ),
                ),
                SizedBox(width: 10),
                DropdownButton<String>(
                  value: chosenValue,
                  //elevation: 5,
                  style: TextStyle(color: Colors.black),

                  items: <String>[
                    'Date(desc)',
                    'Date(asc)',
                    'Relevancy',
                  ].map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(
                        value,
                      ),
                    );
                  }).toList(),
                  onChanged: (String value) {
                    setState(() {
                      chosenValue = value;
                      if (value == 'Date(desc)')
                        unicourtSearchController.seachSortBy.value =
                            'Filing Date';
                      else
                        unicourtSearchController.seachSortBy.value =
                            'Relevancy';
                    });
                    unicourtSearchController.getHttpSearchData();
                  },
                  underline: SizedBox(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

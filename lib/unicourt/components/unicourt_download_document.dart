import 'package:flutter/material.dart';

class UnicourtDownloadDocument extends StatelessWidget {
  const UnicourtDownloadDocument({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Download Documents',
            style: TextStyle(
              fontSize: 18,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Divider(
            color: Colors.grey,
          ),
          SizedBox(
            height: 5,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Table(
              defaultColumnWidth: FixedColumnWidth(140.0),
              children: [
                buildTableHeader(),
                buildLineTableRow(),
                buildTableRow(),
                buildEmptyTableRow(),
                buildTableRow(),
                buildEmptyTableRow(),
                buildTableRow(),
                buildEmptyTableRow(),
                buildTableRow(),
                buildEmptyTableRow(),
                buildTableRow(),
                buildEmptyTableRow(),
                buildTableRow(),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Align(
            alignment: Alignment.center,
            child: TextButton(
              child: Text(
                'Order',
                style: TextStyle(
                  color: Colors.grey[700],
                ),
              ),
              onPressed: () {},
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.grey),
              ),
            ),
          ),
        ],
      ),
    );
  }

  TableRow buildLineTableRow() {
    return TableRow(
      children: [
        Divider(
          color: Colors.grey,
        ),
        Divider(
          color: Colors.grey,
        ),
        Divider(
          color: Colors.grey,
        ),
        Divider(
          color: Colors.grey,
        ),
      ],
    );
  }

  TableRow buildTableRow() {
    return TableRow(
      children: [
        Text(
          'CIVIL COVER SHEET',
          style: TextStyle(
            fontSize: 13,
          ),
        ),
        Text(
          '04/26/2021: CIVIL COVER SHEET',
          style: TextStyle(
            fontSize: 13,
          ),
        ),
        Text(
          '-',
          style: TextStyle(
            fontSize: 13,
          ),
        ),
        Text(
          '\$0.00',
          style: TextStyle(
            fontSize: 13,
          ),
        ),
      ],
    );
  }

  TableRow buildTableHeader() {
    return TableRow(
      children: [
        Text(
          'Document',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey[600],
            fontSize: 15,
          ),
        ),
        Text(
          'Description',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey[600],
            fontSize: 15,
          ),
        ),
        Text(
          'Page(s)',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey[600],
            fontSize: 15,
          ),
        ),
        Text(
          'Cost',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey[600],
            fontSize: 15,
          ),
        ),
      ],
    );
  }

  TableRow buildEmptyTableRow() {
    return TableRow(
      children: [
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
} //class

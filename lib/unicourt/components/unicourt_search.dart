import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class UnicourtSearch extends StatefulWidget {
  UnicourtSearch({Key key}) : super(key: key);

  @override
  _UnicourtSearchState createState() => _UnicourtSearchState();
}

class _UnicourtSearchState extends State<UnicourtSearch> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Container(
        width: double.infinity,
        height: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              flex: 6,
              child: Container(
                padding: EdgeInsets.only(left: 8),
                height: 46,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey,
                    width: 0.2,
                  ),
                ),
                child: InkWell(
                  onTap: () {
                    Future.delayed(
                      Duration(milliseconds: 0),
                      () => Get.toNamed('search'),
                    );
                  },
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text('Search'),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                height: 50,
                color: Colors.green[700],
                child: Icon(
                  Icons.search_outlined,
                  color: Colors.white,
                  size: 35,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

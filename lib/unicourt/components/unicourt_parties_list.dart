import 'package:flutter/material.dart';
import 'package:flutter_ui/unicourt/controllers/unicourt_case_controller.dart';
import 'package:get/get.dart';

class UnicourtPartiesList extends StatefulWidget {
  const UnicourtPartiesList({Key key}) : super(key: key);

  @override
  _UnicourtPartiesListState createState() => _UnicourtPartiesListState();
}

class _UnicourtPartiesListState extends State<UnicourtPartiesList> {
  final UnicourtCaseController unicourtCaseController =
      Get.put(UnicourtCaseController());

  List<dynamic> structuredPartyList = [];
  var partiesCount = 0;

  @override
  void initState() {
    print('init state is calling');
    super.initState();
    getStructuredPartiesList();
  } //init

  getStructuredPartiesList() async {
    unicourtCaseController.partyAttorney.value = [];
    await unicourtCaseController.getAttorneys();
    await unicourtCaseController.getTokens();
    await unicourtCaseController.getParties();

    unicourtCaseController.parties.forEach((element) {
      var fullName = element['fullname'];
      var partyTypes = [];
      partyTypes = element['party_types'];
      partyTypes.forEach((element) {
        var attornyName = '';
        var attorneyIdMain = '';
        var attorneyAssociation = [];
        attorneyAssociation = element['attorney_association'];
        attorneyAssociation.forEach((element) {
          var attorneyId = element['attorney_id'];
          unicourtCaseController.attorneys.forEach((element) {
            if (element['attorney_id'] == attorneyId) {
              attornyName = element['fullname'];
              attorneyIdMain = attorneyId;
            }
          });
        });
        partiesCount++;
        structuredPartyList.add(
          {
            'partyType': '${element['party_type']}',
            'representationType': '${element['representation_type']}',
            'fullName': '$fullName',
            'attorneyName': '$attornyName',
            'attorneyId': '$attorneyIdMain',
          },
        );
      });
    });
    unicourtCaseController.partyAttorney.value = (structuredPartyList);
  } //get strucured party list

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        width: double.infinity,
        child: unicourtCaseController.parties.length == 0
            ? Container(
                child: Align(
                  child: CircularProgressIndicator(),
                ),
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Text.rich(
                    TextSpan(
                        text: '${structuredPartyList.length}',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                        children: [
                          TextSpan(
                            text: ' parties in this case',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ]),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Divider(
                    color: Colors.grey,
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Table(
                      defaultColumnWidth: FixedColumnWidth(140.0),
                      children: [
                        buildTableHeader(),
                        buildTableRowLine(),
                        for (var item in structuredPartyList)
                          TableRow(children: [
                            Container(
                              margin: EdgeInsets.symmetric(vertical: 10),
                              child: Text(
                                item['fullName'],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(vertical: 10),
                              child: Text(
                                item['partyType'],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(vertical: 10),
                              child: Text(
                                item['representationType'],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(vertical: 10),
                              child: Text(
                                item['attorneyName'],
                              ),
                            ),
                          ])
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
      ),
    );
  }

  TableRow buildTableRowLine() {
    return TableRow(
      children: [
        Divider(
          color: Colors.grey,
        ),
        Divider(
          color: Colors.grey,
        ),
        Divider(
          color: Colors.grey,
        ),
        Divider(
          color: Colors.grey,
        ),
      ],
    );
  }

  buildTableData() {
    // unicourtCaseController.parties.forEach((element) {
    //   // buildTableRow('${element['fullname']}', 'Petitioner',
    //   //     'Attorney Represented', 'LORENZANA , MANNY');
    //   // buildEmptyTableRow();
    // });
    buildTableHeader();
  } //buildTableData

  TableRow buildTableRow(
      String partyName, String type, String representation, String attorneys) {
    return TableRow(
      children: [
        Text(
          '$partyName',
          style: TextStyle(
            fontSize: 13,
          ),
        ),
        Text(
          '$type',
          style: TextStyle(
            fontSize: 13,
          ),
        ),
        Text(
          '$representation',
          style: TextStyle(
            fontSize: 13,
          ),
        ),
        Text(
          '$attorneys',
          style: TextStyle(
            fontSize: 13,
          ),
        ),
      ],
    );
  }

  TableRow buildTableHeader() {
    return TableRow(
      children: [
        Text(
          'Party Name',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey[600],
            fontSize: 15,
          ),
        ),
        Text(
          'Type',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey[600],
            fontSize: 15,
          ),
        ),
        Text(
          'Representation',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey[600],
            fontSize: 15,
          ),
        ),
        Text(
          'Attorneys',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey[600],
            fontSize: 15,
          ),
        ),
      ],
    );
  }

  TableRow mytableData() {}

  TableRow buildEmptyTableRow() {
    return TableRow(
      children: [
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
} //class

import 'package:flutter/material.dart';

class ColorPalette {
  final Color primaryColor = Colors.blue[700];
  final Color secondaryColor = Colors.green[700];
}

const searchData = [
  {
    "error": false,
    "message": "OK",
    "data": {
      "result": [
        {
          "case": {
            "case_type": "Motor Vehicle",
            "case_type_category": "Civil",
            "case_type_group": "Personal Injury",
            "case_status_category": "Pending",
            "case_status_name": "Other Pending",
            "courthouse": "Main Branch",
            "state": "FL",
            "jurisdiction": "Florida Palm Beach Court System",
            "last_updated_date": "2021-02-01 17:54:07",
            "last_update_changes_found": "2021-02-01 17:54:07",
            "created_date": "2020-01-13 15:23:40",
            "case_name": "HUNT, WILMA JEAN V FLACK, MATTHEW CHARLES",
            "filing_date": "2020-01-10",
            "case_id": "G5JCMKRRJJGUIMUQHI7XFFDWNRPRW0978",
            "case_number": "50-2020-CA-000307-XXXX-MB",
            "docket":
                "05/08/2020  ... :NOTICE OF TAKING DEPOSITION WILMAJEAN <b>HUNT</b> 07/21/2020 01:00 ... "
          },
          "attorneys": [],
          "parties": [
            {
              "representation_type": "Attorney Represented",
              "party_type": "Petitioner",
              "fullname": "HUNT , WILMA JEAN",
              "entity_type": "Individual"
            },
            {
              "representation_type": "Attorney Represented",
              "party_type": "Plaintiff",
              "fullname": "HUNT , WILMA JEAN",
              "entity_type": "Individual"
            }
          ],
          "judges": []
        },
        {
          "case": {
            "case_type": "Medical Malpractice",
            "case_type_category": "Civil",
            "case_type_group": "Personal Injury",
            "case_status_category": "Pending",
            "case_status_name": "Other Pending",
            "courthouse": "Supreme Court",
            "state": "NY",
            "jurisdiction": "Onondaga County Courts",
            "last_updated_date": "2021-03-11 03:41:45",
            "last_update_changes_found": "2021-03-11 03:41:45",
            "created_date": "2020-01-11 10:36:34",
            "case_name":
                "Tammy A. Hotchkiss et al v. St. Joseph's Hospital Health Center et al",
            "filing_date": "2020-01-10",
            "case_id": "FNDBUHRVHEZCYK4INMZWPB3GLVJRG1029",
            "case_number": "000421/2020",
            "docket":
                "01/29/2020  ... ORAL EXAMINATION; Filed By: <b>Hunt</b>, K.; Filed: 01/29 ... OF PARTICULARS; Filed By: <b>Hunt</b>, K.; Filed: 01/ ...  Omnibus Discovery; Filed By: <b>Hunt</b>, K.; Filed: 01/29 ...  Expert Disclosure; Filed By: <b>Hunt</b>, K.; Filed: 01/29 ... "
          },
          "attorneys": [
            {"firm": "GALE GALE & HUNT", "fullname": "HUNT, KEVIN T."},
            {"firm": "GALE GALE & HUNT, LLC", "fullname": "BAKER, TAYLOR LEE"}
          ],
          "parties": [],
          "judges": []
        },
        {
          "case": {
            "case_type": "Debt Collection",
            "case_type_category": "Civil",
            "case_type_group": "Contract",
            "case_status_category": "Pending",
            "case_status_name": "Other Pending",
            "courthouse": "Supreme Court",
            "state": "NY",
            "jurisdiction": "Dutchess County Courts",
            "last_updated_date": "2021-03-11 02:55:34",
            "last_update_changes_found": "2021-03-11 02:55:34",
            "created_date": "2020-01-11 11:43:05",
            "case_name": "Barclays Bank Delaware v. Julieann Curran",
            "filing_date": "2020-01-10",
            "case_id": "EI6RCFJMGAUSGIT7MIVF47S5KZBQM1020",
            "case_number": "2020-50147",
            "docket":
                "02/11/2021  ... -Julieann Curran-45438894; Filed By: <b>Hunt</b>, J. - filed by Large ...  FORM - PVEF; Filed By: <b>Hunt</b>, J. - filed by Large ... "
          },
          "attorneys": [
            {"firm": "TENAGLIA & HUNT, P.A.", "fullname": "HUNT, JAMES T"},
            {"firm": "Tenaglia & Hunt, P.A.", "fullname": "BERGSTEIN, SHIMON S"}
          ],
          "parties": [],
          "judges": []
        },
        {
          "case": {
            "case_type": "Debt Collection",
            "case_type_category": "Civil",
            "case_type_group": "Contract",
            "case_status_category": "Pending",
            "case_status_name": "Other Pending",
            "courthouse": "Supreme Court",
            "state": "NY",
            "jurisdiction": "Oswego County Courts",
            "last_updated_date": "2021-03-11 03:48:04",
            "last_update_changes_found": "2021-03-11 03:48:04",
            "created_date": "2020-01-11 12:28:24",
            "case_name": "Barclays Bank Delaware v. Dennis M Czeck",
            "filing_date": "2020-01-10",
            "case_id": "EZARKGJQGQWSOJUDMYXGFATBLFEA41024",
            "case_number": "EFC-2020-0053",
            "docket":
                "02/11/2021  ...  M Czeck-45387895; Filed By: <b>Hunt</b>, J. - filed by Large ...  FORM - PVEF; Filed By: <b>Hunt</b>, J. - filed by Large ... "
          },
          "attorneys": [
            {"firm": "TENAGLIA & HUNT, P.A.", "fullname": "HUNT, JAMES T"},
            {"firm": "Tenaglia & Hunt, P.A.", "fullname": "BERGSTEIN, SHIMON S"}
          ],
          "parties": [],
          "judges": []
        },
        {
          "case": {
            "case_type": "Motor Vehicle",
            "case_type_category": "Civil",
            "case_type_group": "Personal Injury",
            "case_status_category": "Pending",
            "case_status_name": "Other Pending",
            "courthouse": "Supreme Court Civil Term - 1",
            "state": "NY",
            "jurisdiction": "Queens County Courts",
            "last_updated_date": "2021-03-11 03:49:21",
            "last_update_changes_found": "2020-01-15 02:09:45",
            "created_date": "2020-01-11 12:38:14",
            "case_name": "Jeffrey Smith v. Denise Hunt",
            "filing_date": "2020-01-10",
            "case_id": "EVABIGBPGMWCMJMCMUWWDALALBFQQ1023",
            "case_number": "700441/2020",
            "docket": ""
          },
          "attorneys": [],
          "parties": [
            {
              "representation_type": "Unrepresented",
              "party_type": "Defendant",
              "fullname": "Denise Hunt",
              "entity_type": "Individual"
            }
          ],
          "judges": []
        },
        {
          "case": {
            "case_type": "Debt Collection",
            "case_type_category": "Civil",
            "case_type_group": "Contract",
            "case_status_category": "Pending",
            "case_status_name": "Other Pending",
            "courthouse": "Supreme Court",
            "state": "NY",
            "jurisdiction": "Tioga County Courts",
            "last_updated_date": "2021-03-11 04:16:00",
            "last_update_changes_found": "2020-01-15 02:14:17",
            "created_date": "2020-01-11 12:42:41",
            "case_name": "Barclays Bank Delaware v. Sarah Dinkins",
            "filing_date": "2020-01-10",
            "case_id": "FBBROGZSGYXSSKEFNAYGJBDDLNIQW1026",
            "case_number": "2020-00061003",
            "docket": ""
          },
          "attorneys": [
            {"firm": "Tenaglia & Hunt, P.A.", "fullname": "BERGSTEIN, SHIMON S"}
          ],
          "parties": [],
          "judges": []
        },
        {
          "case": {
            "case_type": "Debt Collection",
            "case_type_category": "Civil",
            "case_type_group": "Contract",
            "case_status_category": "Pending",
            "case_status_name": "Other Pending",
            "courthouse": "Supreme Court",
            "state": "NY",
            "jurisdiction": "Oneida County Courts",
            "last_updated_date": "2021-03-11 03:39:03",
            "last_update_changes_found": "2021-03-11 03:39:03",
            "created_date": "2020-01-11 14:35:45",
            "case_name": "Barclays Bank Delaware v. Cheryl Danella",
            "filing_date": "2020-01-10",
            "case_id": "E5BBMGRRGUXCQJ4EM4XWHA3CMRGAY1034",
            "case_number": "EFCA2020-000106",
            "docket": ""
          },
          "attorneys": [
            {"firm": "Tenaglia & Hunt, P.A.", "fullname": "BERGSTEIN, SHIMON S"}
          ],
          "parties": [],
          "judges": []
        },
        {
          "case": {
            "case_type": "Debt Collection",
            "case_type_category": "Civil",
            "case_type_group": "Contract",
            "case_status_category": "Pending",
            "case_status_name": "Other Pending",
            "courthouse": "Supreme Court",
            "state": "NY",
            "jurisdiction": "Oneida County Courts",
            "last_updated_date": "2021-03-11 03:39:09",
            "last_update_changes_found": "2020-01-15 04:08:04",
            "created_date": "2020-01-11 14:35:47",
            "case_name": "Barclays Bank Delaware v. Jody S Dorsey",
            "filing_date": "2020-01-10",
            "case_id": "FBBROGZSGYXSSKEFNAYGJBDDMVGQ41035",
            "case_number": "EFCA2020-000107",
            "docket": ""
          },
          "attorneys": [
            {"firm": "Tenaglia & Hunt, P.A.", "fullname": "BERGSTEIN, SHIMON S"}
          ],
          "parties": [],
          "judges": []
        },
        {
          "case": {
            "case_type": "Other",
            "case_type_category": "Civil",
            "case_type_group": "Other",
            "case_status_category": "Pending",
            "case_status_name": "Other Pending",
            "courthouse": "Supreme Court",
            "state": "NY",
            "jurisdiction": "Erie County Courts",
            "last_updated_date": "2021-03-11 02:58:36",
            "last_update_changes_found": "2021-03-11 02:58:36",
            "created_date": "2020-01-11 14:42:58",
            "case_name": "JOHN TOLEDO v. HUNT REAL ESTATE CORPORATION et al",
            "filing_date": "2020-01-10",
            "case_id": "FRDRWHZWHIZS2LEJNQ2GRCDHNFJBK1039",
            "case_number": "800490/2020",
            "docket":
                "02/18/2020 AFFIRMATION/AFFIDAVIT OF SERVICE AOS on <b>Hunt</b> Real Estate via Sec of State; Filed By: Looney, J.; Filed: 02/18/2020; Received: 02/18/2020"
          },
          "attorneys": [],
          "parties": [
            {
              "representation_type": "Attorney Represented",
              "party_type": "Defendant",
              "fullname": "HUNT REAL ESTATE CORPORATION",
              "entity_type": "Company"
            }
          ],
          "judges": []
        },
        {
          "case": {
            "case_type": "Other",
            "case_type_category": "Civil",
            "case_type_group": "Other",
            "case_status_category": "Disposed",
            "case_status_name": "Dismissed",
            "courthouse": "Supreme Court",
            "state": "NY",
            "jurisdiction": "Erie County Courts",
            "last_updated_date": "2021-03-11 02:57:52",
            "last_update_changes_found": "2021-03-11 02:57:52",
            "created_date": "2020-01-11 14:43:34",
            "case_name": "NARENCO, LLC v. RISEN ENERGY AMERICA, INC.",
            "filing_date": "2020-01-10",
            "case_id": "EVABIGBPGMWCMJMCMUWWDALAMJGA61041",
            "case_number": "800481/2020",
            "docket": ""
          },
          "attorneys": [
            {"firm": "GALE GALE & HUNT LLC", "fullname": "GALE, CATHERINE A."}
          ],
          "parties": [],
          "judges": []
        }
      ],
      "total_matches": "179",
      "post_data": {
        "query": [
          {
            "search_terms": ["Hunt"]
          }
        ]
      },
      "criteria": "(0)"
    }
  }
];

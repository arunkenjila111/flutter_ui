class UnicourtConstants {
  static const SEARCH_ATTRIBUTE_LIST = [
    {"Everything": 'All'},
    {"Party": 'Party'},
    {"Attorney": 'Attorney'},
    {"Case Number": 'Case No'},
    {"Case Name": 'Case'},
    {"Judge": 'Judge'},
    {"Docket": 'Docket'},
  ];

  static const SEARCH_SORT_LIST = [
    {"Filing Date": 'Date(desc)'},
    {"Filing Date asc": 'Date(asc)'},
    {"Relevancy": 'Relevance'},
  ];
}

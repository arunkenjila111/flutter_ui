import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PerformanceHome extends StatefulWidget {
  PerformanceHome({Key key}) : super(key: key);

  @override
  _PerformanceHomeState createState() => _PerformanceHomeState();
}

class _PerformanceHomeState extends State<PerformanceHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Performance Home...'),
      ),
      body: Center(
        child: TextButton(
          onPressed: () {
            Get.toNamed('listview');
          },
          child: Text('Go'),
        ),
      ),
    );
  }
} //class

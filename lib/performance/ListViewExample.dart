import 'package:flutter/material.dart';

class ListViewExample extends StatelessWidget {
  const ListViewExample({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'ListView Example',
        ),
      ),
      body: ListView(
        children: [for (var i = 0; i < 10000; i++) BuildWidget()],
      ),
    );
  }
} //class

class BuildWidget extends StatelessWidget {
  const BuildWidget({Key key, this.i}) : super(key: key);
  final int i;

  @override
  Widget build(BuildContext context) {
    return (Padding(
      padding: EdgeInsets.all(10),
      child: Row(
        children: [
          Text('$i'),
        ],
      ),
    ));
  }
}

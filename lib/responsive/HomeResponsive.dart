// created on  28-03-21
// by Arun Kenjila
// demonstrate responsive ui in flutter
//instead of using mediaQuery in every screen, we are creating base screen which makes ui responsive

// 4 things needed
// 1) Orientation 2)DeviceScreenType (mob,tab,pc)
// 3)ScreenSize  4) LocalWidgetSize

// implementation
// a)MediaQuery
// b)LayoutBuilder - size of local widget

import 'package:flutter/material.dart';
import 'package:flutter_ui/responsive/BaseWidget.dart';

class HomeResponsive extends StatefulWidget {
  HomeResponsive({Key key}) : super(key: key);

  @override
  _HomeResponsiveState createState() => _HomeResponsiveState();
}

class _HomeResponsiveState extends State<HomeResponsive> {
  @override
  Widget build(BuildContext context) {
    return BaseWidget(builder: (context, sizingInformation) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Home Responsive....build'),
        ),
        body: Text(
          'Hello',
        ),
      );
    });
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_ui/responsive/SizingInformation.dart';

class BaseWidget extends StatelessWidget {
  final Widget Function(
      BuildContext buildContext, SizingInformation sizingInformation) builder;

  const BaseWidget({Key key, this.builder}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(child: Text('hello'));
  }
}

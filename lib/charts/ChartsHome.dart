import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ui/charts/screens/MyBarChart.dart';
import 'package:flutter_ui/charts/screens/MyLineChart.dart';
import 'package:flutter_ui/charts/screens/MyPieChart.dart';

class ChartsHome extends StatelessWidget {
  const ChartsHome({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Charts',
        ),
      ),
      body: MyPieChart(),
    );
  }
}

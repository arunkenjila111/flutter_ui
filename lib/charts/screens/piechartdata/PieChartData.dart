import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PieData {
  static List<Data> myPieData = [
    Data(name: 'Blue', color: Colors.blue, percent: 40),
    Data(name: 'Orange', color: Colors.orange, percent: 20),
    Data(name: 'Red', color: Colors.red, percent: 10),
    Data(name: 'Yellow', color: Colors.yellow, percent: 30),
  ];
} //class pie data

class Data {
  final String name;
  final Color color;
  final double percent;

  Data({this.name, this.color, this.percent});
} //class data

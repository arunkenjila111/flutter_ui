import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_ui/charts/screens/piechartdata/PieChartData.dart';

class MyPieChart extends StatelessWidget {
  const MyPieChart({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Expanded(
            child: PieChart(
              PieChartData(sections: getSections()),
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: PieData.myPieData.length,
              itemBuilder: (context, index) {
                return Row(
                  children: [
                    Container(
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        color: PieData.myPieData[index].color,
                        shape: BoxShape.circle,
                      ),
                    ),
                    Text('${PieData.myPieData[index].name}'),
                  ],
                );
              },
            ),
          ),
          Expanded(
            child: Text(
              'Hello',
              style: Theme.of(context).textTheme.headline3,
            ),
          ),
        ],
      ),
    );
  }

  List<PieChartSectionData> getSections() {
    return PieData.myPieData
        .asMap()
        .map<int, PieChartSectionData>((index, data) {
          final value = PieChartSectionData(
            color: data.color,
            value: data.percent,
            title: '${data.percent}%',
          );

          return MapEntry(index, value);
        })
        .values
        .toList();
  }
} //end

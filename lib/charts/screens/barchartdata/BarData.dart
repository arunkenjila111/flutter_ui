import 'package:flutter/material.dart';
import 'package:flutter_ui/charts/screens/barchartmodel/Data.dart';

class BarData {
  static int interval = 5;
  static List<Data> barData = [
    Data(
      id: 0,
      name: 'Michal Scott Hunt',
      y: 54,
      color: Colors.blue,
    ),
    Data(
      id: 1,
      name: 'Mark D Waish',
      y: 42,
      color: Colors.blue,
    ),
    Data(
      id: 2,
      name: 'Brian Nevis Winn',
      y: 35,
      color: Colors.blue,
    ),
    Data(
      id: 3,
      name: 'Robert Scott ',
      y: 32,
      color: Colors.blue,
    ),
    Data(
      id: 4,
      name: 'Irwin Jay',
      y: 28,
      color: Colors.blue,
    ),
    Data(
      id: 5,
      name: 'Stebbins',
      y: 14,
      color: Colors.blue,
    ),
    Data(
      id: 0,
      name: 'Michal Scott Hunt',
      y: 54,
      color: Colors.blue,
    ),
    Data(
      id: 6,
      name: 'Mark D Waish',
      y: 42,
      color: Colors.blue,
    ),
    Data(
      id: 7,
      name: 'Brian Nevis Winn',
      y: 35,
      color: Colors.blue,
    ),
    Data(
      id: 8,
      name: 'Robert Scott ',
      y: 32,
      color: Colors.blue,
    ),
    Data(
      id: 9,
      name: 'Irwin Jay',
      y: 28,
      color: Colors.blue,
    ),
    Data(
      id: 10,
      name: 'Stebbins',
      y: 14,
      color: Colors.blue,
    ),
  ];
}

import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

class MyLineChart extends StatelessWidget {
  const MyLineChart({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        padding: EdgeInsets.only(right: 10),
        child: LineChart(
          LineChartData(
              minX: 0,
              maxX: 11,
              minY: 0,
              maxY: 6,
              titlesData: FlTitlesData(
                show: true,
                bottomTitles: SideTitles(
                  showTitles: true,
                  reservedSize: 20,
                  getTitles: (value) {
                    switch (value.toInt()) {
                      case 1:
                        return "JAN";
                      case 3:
                        return "MAR";
                      case 5:
                        return "MAY";
                      case 7:
                        return "JULY";
                      case 9:
                        return "SEP";
                      case 11:
                        return "NOV";
                      default:
                    }
                  },
                ),
                leftTitles: SideTitles(
                  showTitles: true,
                  reservedSize: 20,
                  getTitles: (value) {
                    switch (value.toInt()) {
                      case 1:
                        return "10K";
                      case 3:
                        return "30K";
                      case 5:
                        return "50K";
                      default:
                    }
                  },
                ),
              ),
              lineBarsData: [
                LineChartBarData(
                  spots: [
                    FlSpot(0, 3),
                    FlSpot(2.6, 2),
                    FlSpot(3.3, 2.5),
                    FlSpot(4.2, 4),
                    FlSpot(4.9, 5),
                    FlSpot(6.8, 2.5),
                    FlSpot(8, 4),
                    FlSpot(9.5, 3),
                    FlSpot(11, 4),
                  ],
                  isCurved: false,
                  colors: [Colors.blue, Colors.blue],
                ),
              ]),
        ),
      ),
    );
  }
}

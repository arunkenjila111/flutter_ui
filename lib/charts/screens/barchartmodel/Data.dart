import 'package:flutter/material.dart';

class Data {
  final int id;
  final String name;
  final double y;
  final Color color;

  Data({this.id, this.name, this.y, this.color});
}

import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ui/charts/screens/barchartdata/BarData.dart';

class MyBarChart extends StatelessWidget {
  const MyBarChart({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        padding: EdgeInsets.only(right: 10),
        child: BarChart(
          BarChartData(
            alignment: BarChartAlignment.center,
            maxY: 60,
            groupsSpace: 20,
            titlesData: FlTitlesData(
              leftTitles: SideTitles(
                showTitles: true,
                getTitles: (value) {
                  switch (value.toInt()) {
                    case 0:
                      return "0";
                    case 10:
                      return "10k";
                    case 20:
                      return "20k";
                    case 30:
                      return "30k";
                    case 40:
                      return "40k";
                    case 50:
                      return "50k";
                    case 60:
                      return "60k";
                    default:
                  }
                },
              ),
              bottomTitles: SideTitles(
                margin: 60,
                rotateAngle: -85,
                showTitles: true,
                reservedSize: 50,
                getTitles: (double id) {
                  return BarData.barData
                      .firstWhere((element) => element.id == id.toInt())
                      .name;
                },
              ),
            ),
            gridData: FlGridData(
              checkToShowHorizontalLine: (value) => value % 10 == 0,
              getDrawingHorizontalLine: (value) {
                return FlLine(
                  color: Colors.grey,
                  strokeWidth: 0.4,
                );
              },
            ),
            barGroups: BarData.barData
                .map(
                  (data) => BarChartGroupData(
                    x: data.id,
                    barRods: [
                      BarChartRodData(
                        y: data.y,
                        width: 25,
                        colors: [data.color],
                        borderRadius: data.y > 0
                            ? BorderRadius.only(
                                topLeft: Radius.circular(6),
                                topRight: Radius.circular(6),
                              )
                            : BorderRadius.only(
                                bottomLeft: Radius.circular(6),
                                bottomRight: Radius.circular(6),
                              ),
                      ),
                    ],
                  ),
                )
                .toList(),
          ),
        ),
      ),
    );
  }
}

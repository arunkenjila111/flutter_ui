import 'package:flutter/material.dart';
import 'package:flutter_ui/coffeeshop/constants/color_pallette.dart';
import 'package:flutter_ui/coffeeshop/screens/coffee_header.dart';
import 'package:flutter_ui/coffeeshop/screens/coffee_items.dart';

class CoffeeDashboard extends StatelessWidget {
  const CoffeeDashboard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 4,
      child: Column(
        children: [
          CoffeeHeader(),
          CoffeeItems(),
        ],
      ),
    );
  }
}

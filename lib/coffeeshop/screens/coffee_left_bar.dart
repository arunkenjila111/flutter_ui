import 'package:flutter/material.dart';
import 'package:flutter_ui/coffeeshop/constants/color_pallette.dart';

class CoffeeLeftBar extends StatelessWidget {
  const CoffeeLeftBar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Container(
        padding: EdgeInsets.all(8),
        color: ColorPalette().leftBarColor,
        child: Column(
          children: [
            Icon(
              Icons.menu_outlined,
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class CoffeeHeader extends StatelessWidget {
  const CoffeeHeader({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 2,
      child: Container(
        padding: EdgeInsets.all(8),
        width: double.infinity,
        height: double.infinity,
        color: Colors.blue,
        child: Column(
          children: [
            Align(
              alignment: Alignment.topRight,
              child: Icon(Icons.shop_outlined),
            ),
            Text(
              'Coffee House',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
              ),
            ),
            Text(
              'A lot can happen over coffee',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
              ),
            )
          ],
        ),
      ),
    );
  }
}

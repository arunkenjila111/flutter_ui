import 'package:flutter/material.dart';
import 'package:flutter_ui/carousel/row_dots.dart';
import 'package:flutter_ui/coffeeshop/constants/color_pallette.dart';
import 'package:flutter_ui/coffeeshop/screens/coffee_dashboard.dart';
import 'package:flutter_ui/coffeeshop/screens/coffee_left_bar.dart';

class CoffeeShopHome extends StatelessWidget {
  const CoffeeShopHome({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: [
          CoffeeLeftBar(),
          CoffeeDashboard(),
        ],
      ),
    );
  }
}

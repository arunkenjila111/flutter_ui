import 'package:flutter/material.dart';

class HomeGreen extends StatefulWidget {
  HomeGreen({Key key}) : super(key: key);

  @override
  _HomeGreenState createState() => _HomeGreenState();
}

class _HomeGreenState extends State<HomeGreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Green Ui'),
      ),
      body: Text('Green'),
    );
  }
}

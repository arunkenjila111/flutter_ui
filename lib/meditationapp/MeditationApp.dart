import 'package:flutter/material.dart';

class HomeMeditation extends StatelessWidget {
  const HomeMeditation({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.white.withOpacity(0.9),
      appBar: AppBar(
        elevation: 1,
        title: Text('Meditation'),
        backgroundColor: Color.fromRGBO(227, 157, 125, 1),
        leading: Icon(Icons.menu_outlined),
      ),
      body: Padding(
          padding: const EdgeInsets.all(0),
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              Container(
                height: 200,
                width: double.infinity,
                color: Colors.amber,
                child: Text('Hello'),
              ),
              Positioned(
                bottom: -200,
                left: 0,
                child: Container(
                  color: Colors.transparent,
                  height: 400,
                  width: MediaQuery.of(context).size.width,
                  child: MyGridView(),
                ),
              ),
            ],
          )),
    );
  }
}

class MyGridView extends StatelessWidget {
  const MyGridView({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: GridView.count(
            crossAxisCount: 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 30,
            childAspectRatio: 0.9,
            children: [
              Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.3),
                      blurRadius: 10,
                      offset: Offset(0, 5),
                      spreadRadius: -2,
                    )
                  ],
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.3),
                        blurRadius: 10,
                        offset: Offset(0, 5),
                        spreadRadius: -2,
                      )
                    ]),
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.3),
                        blurRadius: 10,
                        offset: Offset(0, 5),
                        spreadRadius: -2,
                      )
                    ]),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.3),
                      blurRadius: 10,
                      offset: Offset(0, 5),
                      spreadRadius: -2,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceExample extends StatefulWidget {
  SharedPreferenceExample({Key key}) : super(key: key);

  @override
  _SharedPreferenceExampleState createState() =>
      _SharedPreferenceExampleState();
}

class _SharedPreferenceExampleState extends State<SharedPreferenceExample> {
  saveValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt('intKey', 567);
  } //saveValue

  displayValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool checkValue = prefs.containsKey('intKey');
    if (checkValue)
      print(prefs.getInt('intKey'));
    else
      print('No value is present');
  } //display

  removeValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool checkValue = prefs.containsKey('intKey');
    if (checkValue) {
      prefs.remove('intKey');
      print('value removed');
    } else
      print('No value is present');
  } //display

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Shared preference'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Container(
              child: TextButton(
                onPressed: () => saveValue(),
                child: Text(
                  'Save',
                ),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            child: TextButton(
              onPressed: () => displayValue(),
              child: Text('Display'),
            ),
          ),
          Container(
            child: TextButton(
              onPressed: () => removeValue(),
              child: Text('Remove'),
            ),
          ),
        ],
      ),
    );
  }
}

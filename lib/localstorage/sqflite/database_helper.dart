import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final databaseName = "MyDatabase.db";
  static final databaseVersion = 1;

  static final table = 'my_table';

  static final columnId = '_id';
  static final columnName = 'name';
  static final columnAge = 'age';

  static Database database;

  //need to have a getter method to check if database present or not
  //'get' is the keyword used
  Future<Database> get myDatabase async {
    if (database != null) return database;

    return initDabase();
  } //getter method

  Future<Database> initDabase() async {
    Directory directory = await getApplicationSupportDirectory();
    String path = join(directory.path, databaseName);
    return await openDatabase(path,
        version: databaseVersion, onCreate: onCreate);
  } //init

  Future<void> onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE $table (
        $columnId INTEGER PRIMARY KEY,
        $columnName TEXT NOT NULL,
        $columnAge INTEGER NOT NULL
      )
      ''');
  } //oncreate

  //making singleton class
  //so we will get only single instance of this class
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper databaseHelper =
      DatabaseHelper._privateConstructor();

  // insert function
  Future<int> insert(Map<String, dynamic> row) async {
    Database database = await databaseHelper.myDatabase;
    return await database.insert(table, row);
  } //insert

  // display all
  Future<List<Map<String, dynamic>>> displayAll() async {
    // return row as List
    Database database = await databaseHelper.myDatabase;
    return await database.query(table);
  }

  // display specific
  Future<List<Map<String, dynamic>>> displaySpecific(int age) async {
    // return row as List
    Database database = await databaseHelper.myDatabase;
    return await database.query(table, where: "age < ?", whereArgs: [age]);
  }

  //deleting
  Future<int> delete(int id) async {
    //returns no of rows affected
    Database database = await databaseHelper.myDatabase;
    return await database.delete(table, where: "_id = ?", whereArgs: [id]);
  }

  //updating
  Future<int> update(int id) async {
    //returns no of rows affected
    Database database = await databaseHelper.myDatabase;
    return await database.update(table, {"name": "Arun Kenjila Pro"},
        where: "_id = ?", whereArgs: [id]);
  }
} //end

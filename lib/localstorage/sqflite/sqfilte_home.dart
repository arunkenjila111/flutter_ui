import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ui/localstorage/sqflite/database_helper.dart';

class SqlfliteHome extends StatelessWidget {
  SqlfliteHome({Key key}) : super(key: key);
  final databaseHelper = DatabaseHelper.databaseHelper;

  void insertData() async {
    //string -> column name always string
    //dynamic -> column value could be any type
    Map<String, dynamic> row = {
      DatabaseHelper.columnName: 'Zuck',
      DatabaseHelper.columnAge: 21,
    };
    final id = await databaseHelper.insert(row);
    print(id);
  } //insert

  void displayAll() async {
    var allRows = await databaseHelper.displayAll();
    allRows.forEach((element) {
      print(element);
    });
  } //displayAll

  void delete() async {
    var rows = await databaseHelper.delete(5);
    print(rows);
  } //displayAll

  void update() async {
    var rows = await databaseHelper.update(3);
    print(rows);
  } //displayAll

  void displaySpecific() async {
    var allRows = await databaseHelper.displaySpecific(20);
    print('Specific rows');
    allRows.forEach((element) {
      print(element);
    });
  } //displayAll

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ios Style'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Center(
            child: TextButton(
              onPressed: () {
                insertData();
              },
              child: Text('INSERT'),
            ),
          ),
          TextButton(
            onPressed: () {
              delete();
            },
            child: Text('DELETE'),
          ),
          TextButton(
            onPressed: () {
              update();
            },
            child: Text('UPDATE'),
          ),
          TextButton(
            onPressed: () {
              displayAll();
            },
            child: Text('DISPLAY'),
          ),
        ],
      ),
    );
  }
}

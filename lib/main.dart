import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ui/animation/animation_home.dart';
import 'package:flutter_ui/background/background_button.dart';
import 'package:flutter_ui/background/background_home.dart';
import 'package:flutter_ui/charts/ChartsHome.dart';
import 'package:flutter_ui/coffeeshop/coffee_shop_home.dart';
import 'package:flutter_ui/ecommerce/EcommerceScreen.dart';
import 'package:flutter_ui/flutterweb/web_example.dart';
import 'package:flutter_ui/formvalidation/FormValidationHome.dart';
import 'package:flutter_ui/furnitureApp/screens/product/ProductDetailScreen.dart';
import 'package:flutter_ui/furnitureApp/screens/product/ProductScreen.dart';
import 'package:flutter_ui/localstorage/shared_preference_example.dart';
import 'package:flutter_ui/localstorage/sqflite/sqfilte_home.dart';
import 'package:flutter_ui/location/location_home.dart';
import 'package:flutter_ui/mastercard/pages/mastercard_home.dart';
import 'package:flutter_ui/meditationapp/MeditationApp.dart';
import 'package:flutter_ui/performance/ListViewExample.dart';
import 'package:flutter_ui/performance/PerformanceHome.dart';
import 'package:flutter_ui/resonsive2/HomeResponsive2.dart';
import 'package:flutter_ui/unicourt/screens/unicourt_home.dart';
import 'package:flutter_ui/unicourt/screens/unicourt_search_detail_screen.dart';
import 'package:flutter_ui/unicourt/screens/unicourt_search_screen.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_ui/furnitureApp/constants.dart';
import 'package:get/get.dart';
import 'package:workmanager/workmanager.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material',
      initialRoute: '/',
      defaultTransition: Transition.native,
      getPages: [
        GetPage(
          name: "/",
          page: () => UnicourtHome(),
        ),
        GetPage(
          name: "search",
          page: () => UnicourtSearchScreen(),
        ),
        GetPage(
          name: "searchDetail",
          page: () => UnicourtSearchDetailScreen(),
        ),
      ],
    );
  }
}

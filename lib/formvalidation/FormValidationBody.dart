import 'package:flutter/material.dart';

class FormValidationBody extends StatefulWidget {
  FormValidationBody({Key key}) : super(key: key);

  @override
  _FormValidationBodyState createState() => _FormValidationBodyState();
}

class _FormValidationBodyState extends State<FormValidationBody> {
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        children: [
          TextFormField(
            validator: (value) {
              if (value.length == 0) return "Name should not be empty";
              if (value.length == 1) return "Name should have atleast 2 chars";
            },
            decoration: InputDecoration(
              hintText: 'Enter Name',
              helperText: '',
            ),
          ),
          TextFormField(
            validator: (value) {
              if (value.length == 0) return "email should not be empty";
            },
            decoration: InputDecoration(
              hintText: 'Enter email',
              helperText: '',
            ),
          ),
          TextButton(
            onPressed: () {
              formKey.currentState.validate()
                  ? ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text("Login success"),
                      ),
                    )
                  : ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text("Login failed... see the errors"),
                      ),
                    );
            },
            child: Text(
              'SUBMIT',
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_ui/formvalidation/FormValidationBody.dart';

class FormValidationHome extends StatefulWidget {
  FormValidationHome({Key key}) : super(key: key);

  @override
  _FormValidationHomeState createState() => _FormValidationHomeState();
}

class _FormValidationHomeState extends State<FormValidationHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Form Validation'),
      ),
      body: FormValidationBody(),
    );
  }
}

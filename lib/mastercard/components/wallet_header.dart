import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ui/mastercard/constants/data.dart';

class WalletHeader extends StatelessWidget {
  const WalletHeader({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Arun Kenjila',
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.w600,
              ),
            ),
            Container(
              child: Icon(Icons.notifications),
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                color: primaryColor,
                shape: BoxShape.circle,
                boxShadow: customShadow,
                // border: Border.all(
                //   color: Colors.deepOrange,
                //   width: 5,
                // ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

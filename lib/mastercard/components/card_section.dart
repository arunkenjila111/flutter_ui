import 'package:flutter/material.dart';
import 'package:flutter_ui/mastercard/components/card_details.dart';
import 'package:flutter_ui/mastercard/constants/data.dart';

class CardSection extends StatelessWidget {
  const CardSection({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        flex: 3,
        child: Container(
          padding: EdgeInsets.all(4),
          child: Column(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Selected Card',
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: 5,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return Container(
                        margin: EdgeInsets.all(10),
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: primaryColor,
                          boxShadow: customShadow,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Stack(
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          children: [
                            Positioned.fill(
                              top: 150,
                              bottom: -200,
                              child: Container(
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white24,
                                    shape: BoxShape.circle,
                                    boxShadow: customShadow,
                                  ),
                                ),
                              ),
                            ),
                            Positioned.fill(
                              top: -100,
                              left: -300,
                              bottom: -100,
                              child: Container(
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white30,
                                    shape: BoxShape.circle,
                                    boxShadow: customShadow,
                                  ),
                                ),
                              ),
                            ),
                            CardDetails(),
                          ],
                        ),
                      );
                    }),
              ),
            ],
          ),
        ));
  }
}

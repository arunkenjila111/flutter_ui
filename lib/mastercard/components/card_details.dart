import 'package:flutter/material.dart';

class CardDetails extends StatelessWidget {
  const CardDetails({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 100,
          child: Image.asset(
            'assets/images/mastercard.png',
            fit: BoxFit.cover,
          ),
        ),
        Positioned(
          bottom: -7,
          left: 0,
          child: Column(
            children: [
              Text(
                "**** **** **** 1930",
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}

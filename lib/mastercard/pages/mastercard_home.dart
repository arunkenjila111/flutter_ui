import 'package:flutter/material.dart';
import 'package:flutter_ui/mastercard/components/card_section.dart';
import 'package:flutter_ui/mastercard/components/wallet_header.dart';
import 'package:flutter_ui/mastercard/constants/data.dart';

class MasterCardHome extends StatelessWidget {
  const MasterCardHome({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primaryColor,
      appBar: AppBar(
        title: Text('master card home'),
      ),
      body: Column(
        children: [
          WalletHeader(),
          CardSection(),
          Expanded(
            flex: 3,
            child: Container(
              color: Colors.blue,
            ),
          ),
        ],
      ),
    );
  }
}

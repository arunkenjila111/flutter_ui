import 'package:flutter/material.dart';

class CustomAnimation extends StatelessWidget {
  const CustomAnimation({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TweenAnimationBuilder<double>(
      tween: Tween<double>(begin: 0, end: 2 * 3.14),
      duration: Duration(seconds: 2),
      builder: (BuildContext context, double angle, Widget child) {
        return Transform.rotate(
          angle: angle,
          child: Image.network(
              'https://cdn.worldvectorlogo.com/logos/flutter-logo.svg'),
        );
      },
    );
  }
}

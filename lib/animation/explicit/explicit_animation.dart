import 'package:flutter/material.dart';

class ExplicitAnimation extends StatefulWidget {
  ExplicitAnimation({Key key}) : super(key: key);

  @override
  _ExplicitAnimationState createState() => _ExplicitAnimationState();
}

class _ExplicitAnimationState extends State<ExplicitAnimation>
    with TickerProviderStateMixin {
  AnimationController animationController;
  Animation<Color> colorAnimation;
  Animation<double> sizeAnimation;

  bool isClicked = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    animationController = AnimationController(
      duration: Duration(milliseconds: 300),
      vsync: this, //used to sync the animation with ticker
    );

    //color animation
    colorAnimation = ColorTween(begin: Colors.grey[400], end: Colors.red)
        .animate(animationController);

    //start the animation
    // by default value will change from 0 to 1
    // animationController.forward();

    //size animation
    sizeAnimation = TweenSequence(<TweenSequenceItem<double>>[
      TweenSequenceItem(
        tween: Tween<double>(
          begin: 30,
          end: 50,
        ),
        weight: 1,
      ),
      TweenSequenceItem(
        tween: Tween<double>(
          begin: 50,
          end: 30,
        ),
        weight: 1,
      ),
    ]).animate(animationController);

    //listening to value changing in the duration of 10 seconds
    animationController.addListener(() {
      print(animationController.value);
      print(colorAnimation.value);
    });

    //listeting for status to reverse the animation
    animationController.addStatusListener((status) {
      isClicked = status == AnimationStatus.completed ? true : false;
    });
  } //initState

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AnimatedBuilder(
        animation: animationController,
        builder: (BuildContext context, Widget child) {
          return IconButton(
            icon: Icon(
              Icons.favorite,
              size: sizeAnimation.value,
              color: colorAnimation.value,
            ),
            onPressed: () {
              isClicked
                  ? animationController.reverse()
                  : animationController.forward();
            },
          );
        },
      ),
    );
  }
}

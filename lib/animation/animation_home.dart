import 'package:flutter/material.dart';
import 'package:flutter_ui/animation/custom_animation.dart';
import 'package:flutter_ui/animation/explicit/explicit_animation.dart';

import 'built_in_animation.dart';

class AnimationHome extends StatefulWidget {
  const AnimationHome({Key key}) : super(key: key);

  @override
  _AnimationHomeState createState() => _AnimationHomeState();
}

class _AnimationHomeState extends State<AnimationHome> {
  Color color = Colors.red;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Animation Home'),
      ),
      body: ExplicitAnimation(),
    );
  }
}

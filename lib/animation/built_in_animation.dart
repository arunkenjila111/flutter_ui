import 'package:flutter/material.dart';

class BuiltInAnimation extends StatefulWidget {
  const BuiltInAnimation({
    Key key,
  });

  @override
  _BuiltInAnimationState createState() => _BuiltInAnimationState();
}

class _BuiltInAnimationState extends State<BuiltInAnimation> {
  double _width = 200;
  Color color = Colors.red;
  double opacity = 1;
  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 100),
      color: color,
      width: _width,
      child: Column(
        children: [
          TextButton(
            onPressed: () {
              setState(() {
                color = Colors.blue;
              });
            },
            child: Text('Change opacity'),
          ),
        ],
      ),
    );
  }
}

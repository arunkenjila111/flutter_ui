import 'package:flutter/material.dart';

class WebExample extends StatefulWidget {
  WebExample({Key key}) : super(key: key);

  @override
  _WebExampleState createState() => _WebExampleState();
}

class _WebExampleState extends State<WebExample> {
  int counter = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Web Example'),
      ),
      body: Column(
        children: [
          Center(
            child: Text(
              '$counter',
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          incrementCounter();
        },
        child: Text('ADD'),
      ),
    );
  } //build

  void incrementCounter() {
    setState(() {
      counter++;
    });
  }
} //class

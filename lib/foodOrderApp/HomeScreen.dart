import 'package:flutter/material.dart';

class FoodHome extends StatefulWidget {
  FoodHome({Key key}) : super(key: key);

  _FoodHomeState createState() => _FoodHomeState();
}

class _FoodHomeState extends State<FoodHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         backgroundColor: Colors.white,
         leading: Icon(Icons.menu_outlined,color:Colors.black),
         title:  Row(
           mainAxisAlignment: MainAxisAlignment.spaceBetween,
           children: [
             SizedBox(
               width: 10,
             ),
             CircleAvatar(
               radius: 18,
               backgroundImage: NetworkImage('https://lh3.googleusercontent.com/proxy/qQQnZxBSfvo6fgtI6cUaZ0LuoBMyfq4UcOR4H_eAdQ9EZ93rRdguYvVtasw7xYaohVGbJ-wznVVXoaE1beXEAy7bfPaVjYSD4MP1s3LOFEiAgFHrmRE9PULGigHvIjuO5uH7'),
               backgroundColor: Colors.transparent,
             )
           ],
         ) 
       ),
       body: Padding(
         padding: EdgeInsets.all(15),
         child: Expanded(
                    child: Column(
             children: [
               SearchBar(),
               SizedBox(height: 20,),
               ListView(
                              children: [
                    Container(
                   height: 185,
                   width: 135,
                   decoration: BoxDecoration(
                     color: Colors.red.withOpacity(0.2),
                     borderRadius: BorderRadius.circular(10),
                   ),
                   child: Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: Column(
                       mainAxisAlignment: MainAxisAlignment.spaceAround,
                       children: [
                         Text('Samosa'),
                         Text('Samosa'),
                         Text('Samosa'),
                         Text('Samosa'),

                       ],
                     ),
                   ),
                 ),]
               )
             ],
           ),
         ),

       )
    );
  }
}

class SearchBar extends StatelessWidget {
  const SearchBar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children : [
    Text('SEARCH FOR',style: TextStyle(fontWeight: FontWeight.w700),),
     SizedBox(height: 10,),
     Text('RECIPES',style: TextStyle(fontWeight: FontWeight.w700)),
     SizedBox(height: 10,),
     Container(
       decoration: BoxDecoration(
         color: Colors.grey.withOpacity(0.1),
         borderRadius: BorderRadius.circular(10)
       ),
       child: TextField(
         decoration: InputDecoration(
           hintText: 'Search',
           border: InputBorder.none,
           fillColor: Colors.grey.withOpacity(0.5),
           prefixIcon: Icon(Icons.search)
         ),
         
       ),
     )

    ]   
    
    );
  }
}
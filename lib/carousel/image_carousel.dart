import 'package:flutter/material.dart';
import 'package:flutter_ui/carousel/row_dots.dart';

class ImageCarousel extends StatefulWidget {
  ImageCarousel({Key key}) : super(key: key);

  _ImageCarouselState createState() => _ImageCarouselState();
}

class _ImageCarouselState extends State<ImageCarousel> {

  int photoIndex = 0;

  List<String> photos = ['assets/image1.png','assets/image2.png','assets/image3.png','assets/image4.png'];

  void getPreviousImage(){
    setState(() {
       photoIndex = photoIndex > 0 ? photoIndex - 1 : 0; 
    });
  }//prev

  void getNextImage(){
    setState(() {
       photoIndex = photoIndex < 3 ? photoIndex + 1 : photoIndex; 
    });
  }//next


  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
         Stack(
                children:[ 
                  Container(
                    height: 320,
                    width: 300,
                    child: Image.asset(photos[photoIndex],fit:BoxFit.contain,)
                  ),
                  Positioned(
                    child: RowDots(numberOfDots: photos.length,photoIndex: photoIndex,),
                    top: 300,
                    left: 150,
                  ),
                ]  
         ),
         Row(
           mainAxisAlignment: MainAxisAlignment.center,
           children: [
             ElevatedButton(
               onPressed: (){
                 getNextImage();
               },
               child: Text('Next'),
             ),
             SizedBox(width: 30,),
             ElevatedButton(
               onPressed: (){
                 getPreviousImage();
               },
               child: Text('Prev'),
             )
           ],
         )
      ],
    );
  }
}
import 'package:flutter/material.dart';

class RowDots extends StatelessWidget {

  final int numberOfDots;
  final int photoIndex;
  const RowDots({Key key,this.numberOfDots,this.photoIndex}) : super(key: key);

  Widget activeDot()
  {
     return Container(
       child: Padding(
         padding: EdgeInsets.only(left: 3,right: 3),
         child: Container(
           height: 12,
           width: 12,
           decoration: BoxDecoration(
             color: Colors.blue,
             borderRadius: BorderRadius.circular(6),
           ),
         ),
       ),
     );
  }//activeDot


   Widget inActiveDot()
  {
     return Container(
       child: Padding(
         padding: EdgeInsets.only(left: 3,right: 3),
         child: Container(
           height: 8,
           width: 8,
           decoration: BoxDecoration(
             color: Colors.grey,
             borderRadius: BorderRadius.circular(4)
           ),
         ),
       ),
     );
  }//inActiveDot

  
  //method to dispay row dots
  List<Widget> getDots()
  {
      List<Widget> dots = [];

      for(int i=0;i<numberOfDots;i++)
        dots.add(
          i == photoIndex ? activeDot() : inActiveDot()
        );

      return dots;  
  }//getDots

  @override
  Widget build(BuildContext context) {
    return Container(
     child : Row (
     children: 
        getDots(),

      )
    );
  }
}
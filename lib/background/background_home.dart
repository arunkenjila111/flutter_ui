import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class BackgroundHome extends StatefulWidget {
  const BackgroundHome({Key key}) : super(key: key);

  @override
  _BackgroundHomeState createState() => _BackgroundHomeState();
}

int add(int a) {
  return 10 + a;
}

getFromApi(Uri message) async {
  final response = await http.get(
    message,
  );
  var result = [];

  result = jsonDecode(response.body);

  for (var item in result) {
    print(item);
  }
  return result;
} //method

class _BackgroundHomeState extends State<BackgroundHome> {
  var photoList = [];
  var myText = '10';

  fetchPhotos() async {
    var result = await compute(
        getFromApi, Uri.parse('https://jsonplaceholder.typicode.com/photos'));
    setState(() {
      photoList = result;
    });
  } //fetch

  normalFetchPhotos() async {
    final response = await http.get(
      Uri.parse('https://jsonplaceholder.typicode.com/photos'),
    );
    var result = [];

    result = jsonDecode(response.body);

    setState(() {
      photoList = result;
    });
  } //normal

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchPhotos();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Background Home'),
      ),
      body: ListView.builder(
          itemCount: photoList.length,
          itemBuilder: (context, index) {
            return (Container(
              child: Text('${photoList[index]['title']}'),
            ));
          }),
    );
  }
}

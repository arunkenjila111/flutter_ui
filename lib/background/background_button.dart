import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BackgroundButton extends StatelessWidget {
  const BackgroundButton({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Background'),
      ),
      body: Center(
        child: TextButton(
          onPressed: () {
            Get.toNamed('bg');
          },
          child: Text('Go to'),
        ),
      ),
    );
  }
}

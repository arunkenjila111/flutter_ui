import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const mBackgroundColor = Color(0xFFF1EFF1);
const mPrimaryColor = Color(0xFF035AA6);
const mSecondaryColor = Color(0xFFFFA41B);
const mTextColor = Color(0xFF000839);
const mTextLightColor = Color(0xFF747474);
const mBlueColor = Color(0xFF40BAD5);

const mDefaultPadding = 20.0;

const mDefaultShadow = BoxShadow(
  offset: Offset(0, 15),
  blurRadius: 27,
  color: Colors.black,
);

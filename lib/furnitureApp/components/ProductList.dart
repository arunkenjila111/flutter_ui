import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../constants.dart';
import 'ProductSingleCard.dart';
import '../models/Product.dart';

class ProductList extends StatefulWidget {
  ProductList({Key key}) : super(key: key);

  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.only(top: 50),
          decoration: BoxDecoration(
            color: mBackgroundColor,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(40),
              topRight: Radius.circular(40),
            ),
          ),
        ),
        ListView.builder(
          itemCount: products.length,
          itemBuilder: (context, index) {
            return ProductSingleCard(
              index: index,
              title: products[index].title,
              price: products[index].price,
              imageUrl: products[index].imageUrl,
            );
          },
        )
      ],
    );
  }
}

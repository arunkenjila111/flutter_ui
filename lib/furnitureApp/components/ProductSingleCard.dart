import 'package:flutter/material.dart';
import 'package:flutter_ui/furnitureApp/models/Product.dart';
import 'package:get/get.dart';
import '../constants.dart';

class ProductSingleCard extends StatelessWidget {
  const ProductSingleCard({
    Key key,
    this.title,
    this.price,
    this.imageUrl,
    this.press,
    this.index,
  }) : super(key: key);

  final String title, imageUrl;
  final int price, index;
  final Function press;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Get.toNamed(
          'product_detail',
          arguments: products[index],
        );
      },
      child: Container(
        margin: EdgeInsets.symmetric(
          vertical: mDefaultPadding,
          horizontal: mDefaultPadding * 1.5,
        ),
        height: 160,
        decoration: BoxDecoration(
          color: Colors.red,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Row(
          children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    bottomLeft: Radius.circular(20),
                  ),
                ),
                child: Column(
                  children: [
                    Expanded(
                      flex: 4,
                      child: Container(
                        padding: EdgeInsets.all(8),
                        child: Center(
                          child: Text(
                            '$title',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.bottomLeft,
                        child: Container(
                          padding: EdgeInsets.all(4),
                          width: 80,
                          decoration: BoxDecoration(
                            color: mSecondaryColor,
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(20),
                              topRight: Radius.circular(25),
                            ),
                          ),
                          child: Center(
                            child: Text(
                              '$price\$',
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Stack(
                clipBehavior: Clip.none,
                alignment: Alignment.center,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(20),
                        bottomRight: Radius.circular(20),
                      ),
                    ),
                  ),
                  Positioned(
                    top: -20,
                    child: Container(
                      height: 150,
                      child: Hero(
                        tag: '$price+$imageUrl',
                        child: Image.network(
                          '$imageUrl',
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_ui/furnitureApp/components/ProductList.dart';

class MyTabBar extends StatefulWidget {
  MyTabBar({Key key}) : super(key: key);

  @override
  _MyTabBarState createState() => _MyTabBarState();
}

class _MyTabBarState extends State<MyTabBar>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    tabController = TabController(length: 4, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TabBar(
          isScrollable: true,
          controller: tabController,
          tabs: [
            Tab(
              text: 'All',
            ),
            Tab(
              text: 'Sofa',
            ),
            Tab(
              text: 'Park Bench',
            ),
            Tab(
              text: 'Arm Chair',
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Expanded(
          child: TabBarView(
            controller: tabController,
            children: [
              ProductList(),
              Text('Sofa'),
              Text('Park Bench'),
              Text('Arm Chair'),
            ],
          ),
        ),
      ],
    );
  }
} //class

class Product {
  final int id, price;
  final String title, description, imageUrl;

  Product({this.id, this.price, this.title, this.description, this.imageUrl});
}

List<Product> products = [
  Product(
    id: 1,
    price: 67,
    title: 'Classic Leather Arm Chair',
    description: 'My Description',
    imageUrl: 'https://pngimg.com/uploads/chair/chair_PNG6844.png',
  ),
  Product(
    id: 2,
    price: 56,
    title: 'Royal Chair',
    description: 'My Description',
    imageUrl:
        'https://lh3.googleusercontent.com/proxy/dbC-A_tSwjIeAFkSV0whZ2LL6F3PwT08qIFZ4LGPEYwMfHpdv-cHuPTFmm88peGesSpQh4ZlwQ0BQjYVXzH0k-c-Y5lI_bz1eazXcXMgshi5S1OggL9CDGUgjbmpFb4',
  ),
  Product(
    id: 3,
    price: 95,
    title: 'Classic Leather Arm Chair',
    description: 'My Description',
    imageUrl: 'https://www.freeiconspng.com/thumbs/chair-png/chair-png-30.png',
  ),
  Product(
    id: 4,
    price: 67,
    title: 'C lassic Leather Arm Chair',
    description: 'My Description',
    imageUrl:
        'https://www.freepnglogos.com/uploads/chair-png/wooden-chair-png-transparent-image-pngpix-0.png',
  ),
  Product(
    id: 5,
    price: 180,
    title: 'Classic Leather Arm Chair',
    description: 'My Description',
    imageUrl:
        'https://www.freepnglogos.com/uploads/chair-png/wooden-chair-png-transparent-image-pngpix-0.png',
  ),
];

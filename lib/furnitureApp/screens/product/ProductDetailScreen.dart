import 'package:flutter/material.dart';
import 'package:flutter_ui/furnitureApp/constants.dart';
import 'package:flutter_ui/furnitureApp/screens/product/components/ProductDetailsBody.dart';
import 'package:get/get.dart';

class ProductDetailScreen extends StatefulWidget {
  ProductDetailScreen({Key key}) : super(key: key);

  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  @override
  Widget build(BuildContext context) {
    final String title = Get.arguments.title;
    final String imageUrl = Get.arguments.imageUrl;
    final int price = Get.arguments.price;

    return Scaffold(
      backgroundColor: mPrimaryColor,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_outlined),
          onPressed: () {
            Get.back();
          },
          color: Colors.black.withOpacity(0.6),
        ),
        elevation: 0,
        backgroundColor: mBackgroundColor,
        title: Text(
          'Back',
          style: TextStyle(
            color: Colors.black.withOpacity(0.6),
          ),
        ),
      ),
      body: ProductDetailsBody(
        title: title,
        price: price,
        imageUrl: imageUrl,
      ),
    );
  }
} //class

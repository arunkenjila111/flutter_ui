import 'package:flutter/material.dart';
import 'package:flutter_ui/furnitureApp/constants.dart';
import 'package:flutter_ui/furnitureApp/screens/product/components/ProductBody.dart';

class ProductScreen extends StatefulWidget {
  ProductScreen({Key key}) : super(key: key);

  @override
  _ProductScreenState createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: mPrimaryColor,
      appBar: AppBar(
        backgroundColor: mPrimaryColor,
        elevation: 0,
        title: Text('Dashboard'),
        actions: [
          IconButton(
            icon: Icon(Icons.notifications_outlined),
            onPressed: () {},
          )
        ],
      ),
      body: ProductBody(),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_ui/furnitureApp/constants.dart';

class ProductDetailsBody extends StatelessWidget {
  const ProductDetailsBody({Key key, this.title, this.imageUrl, this.price})
      : super(key: key);
  final String title, imageUrl;
  final int price;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Column(
      children: [
        Expanded(
          flex: 4,
          child: Container(
            decoration: BoxDecoration(
              color: mBackgroundColor,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(50),
                bottomRight: Radius.circular(50),
              ),
            ),
            child: Column(
              children: [
                Expanded(
                    flex: 3,
                    child: Stack(
                      children: [
                        Center(
                          child: Container(
                            height: size.width * 0.7,
                            width: size.width * 0.7,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.circle,
                            ),
                          ),
                        ),
                        Center(
                          child: Container(
                            height: size.width * 0.8,
                            width: size.width * 0.8,
                            child: Hero(
                              tag: '$price+$imageUrl',
                              child: Image.network(
                                '$imageUrl',
                                fit: BoxFit.fitHeight,
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
                Expanded(
                  flex: 1,
                  child: Container(
                    child: Text(
                      '$title',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Expanded(
            flex: 1,
            child: Center(
              child: Container(
                margin: EdgeInsets.all(20),
                width: size.width,
                height: 60,
                decoration: BoxDecoration(
                  color: mSecondaryColor,
                  borderRadius: BorderRadius.circular(30),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Order Now',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                    Icon(
                      Icons.save_alt_rounded,
                      size: 30,
                    ),
                  ],
                ),
              ),
            )),
      ],
    );
  }
}

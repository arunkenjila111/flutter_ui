import 'package:flutter/material.dart';
import 'package:flutter_ui/furnitureApp/components/MySearchBar.dart';
import 'package:flutter_ui/furnitureApp/components/MyTabBar.dart';
import 'package:flutter_ui/furnitureApp/constants.dart';

class ProductBody extends StatelessWidget {
  const ProductBody({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          flex: 1,
          child: MySearchBar(),
        ),
        Expanded(
          flex: 6,
          child: MyTabBar(),
        )
      ],
    );
  }
} //class

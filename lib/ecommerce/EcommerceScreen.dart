import 'package:flutter/material.dart';
import 'package:flutter_ui/ecommerce/components/EcommerceBody.dart';

class EcommerceHome extends StatefulWidget {
  EcommerceHome({Key key}) : super(key: key);

  @override
  _EcommerceHomeState createState() => _EcommerceHomeState();
}

class _EcommerceHomeState extends State<EcommerceHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: buildAppBar(),
      body: EcommerceBody(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: Colors.white,
      leading: Icon(
        Icons.menu_outlined,
        color: Colors.grey,
      ),
      title: Text(
        'E commerce',
        style: TextStyle(
          color: Colors.grey,
        ),
      ),
      actions: [
        Container(
          height: 40,
          width: 40,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Color.fromRGBO(220, 222, 220, 1),
          ),
          child: Center(
            child: Icon(
              Icons.search_outlined,
              color: Colors.grey,
            ),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Container(
          height: 40,
          width: 40,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Color.fromRGBO(220, 222, 220, 1),
          ),
          child: Center(
            child: Icon(
              Icons.shopping_basket_outlined,
              color: Colors.grey,
            ),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Container(
          height: 40,
          width: 40,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Color.fromRGBO(220, 222, 220, 1),
          ),
          child: Center(
            child: Icon(
              Icons.notifications_outlined,
              color: Colors.grey,
            ),
          ),
        ),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }
}

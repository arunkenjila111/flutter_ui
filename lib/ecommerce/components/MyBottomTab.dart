import 'package:flutter/material.dart';
import 'package:flutter_ui/carousel/row_dots.dart';

class MyBottomTab extends StatefulWidget {
  const MyBottomTab({
    Key key,
  }) : super(key: key);

  @override
  _MyBottomTabState createState() => _MyBottomTabState();
}

class _MyBottomTabState extends State<MyBottomTab>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    // TODO: implement initState
    tabController = TabController(length: 4, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            child: Column(
              children: [
                Icon(
                  Icons.home_outlined,
                ),
                Text(
                  'Home',
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: [
                Icon(
                  Icons.gamepad_outlined,
                ),
                Text(
                  'Games',
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: [
                Icon(
                  Icons.person_outline_outlined,
                ),
                Text(
                  'Profile',
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: [
                Icon(
                  Icons.settings_outlined,
                ),
                Text(
                  'Settings',
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

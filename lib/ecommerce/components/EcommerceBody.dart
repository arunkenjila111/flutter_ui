import 'package:flutter/material.dart';
import 'package:flutter_ui/ecommerce/components/MyBanner.dart';

import 'MyBottomTab.dart';
import 'MyCategory.dart';
import 'PopularProduct.dart';
import 'SpecialOffer.dart';

class EcommerceBody extends StatelessWidget {
  EcommerceBody({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        MyBanner(),
        MyCategory(),
        SpecialOffer(),
        PopularProduct(),
        MyBottomTab(),
      ],
    );
  }
}

import 'package:flutter/material.dart';

class MyCategory extends StatelessWidget {
  MyCategory({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 2,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SingleCategory(
            title: 'Offers',
            icon: Icon(
              Icons.local_offer_outlined,
              color: Color.fromRGBO(232, 38, 16, 1),
            ),
          ),
          SingleCategory(
            title: 'Bill',
            icon: Icon(
              Icons.payment_outlined,
              color: Color.fromRGBO(232, 38, 16, 1),
            ),
          ),
          SingleCategory(
            title: 'Games',
            icon: Icon(
              Icons.games_outlined,
              color: Color.fromRGBO(232, 38, 16, 1),
            ),
          ),
          SingleCategory(
            title: 'Gift',
            icon: Icon(
              Icons.card_giftcard_outlined,
              color: Color.fromRGBO(232, 38, 16, 1),
            ),
          ),
          SingleCategory(
            title: 'More ',
            icon: Icon(
              Icons.more_outlined,
              color: Color.fromRGBO(232, 38, 16, 1),
            ),
          ),
        ],
      ),
    );
  }
}

class SingleCategory extends StatelessWidget {
  const SingleCategory({
    Key key,
    this.title,
    this.icon,
  }) : super(key: key);

  final String title;
  final Icon icon;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 60,
        height: 60,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              width: 50,
              height: 35,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color.fromRGBO(230, 69, 93, 1).withOpacity(0.3),
              ),
              child: icon,
            ),
            Text(
              '$title',
              maxLines: 2,
            )
          ],
        ));
  }
}

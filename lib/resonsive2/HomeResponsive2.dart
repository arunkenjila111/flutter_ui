import 'package:flutter/material.dart';
import 'package:flutter_ui/resonsive2/screens/ChatInfo.dart';
import 'package:flutter_ui/resonsive2/screens/ChatUsers.dart';
import 'package:flutter_ui/resonsive2/screens/MyDeviceType.dart';
import 'package:flutter_ui/resonsive2/screens/SideMenu.dart';

class HomeResponsive2 extends StatefulWidget {
  HomeResponsive2({Key key}) : super(key: key);

  @override
  _HomeResponsive2State createState() => _HomeResponsive2State();
}

class _HomeResponsive2State extends State<HomeResponsive2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('Home Responsive 2'),
      // ),
      body: MyDeviceType(
        mobile: Container(
          child: Row(
            children: [
              Expanded(
                flex: 1,
                child: ChatUsers(),
              ),
            ],
          ),
        ),
        tablet: Container(
          child: Row(
            children: [
              Expanded(
                flex: 4,
                child: ChatUsers(),
              ),
              Expanded(
                flex: 6,
                child: ChatInfo(),
              ),
            ],
          ),
        ),
        desktop: Container(
          child: Row(
            children: [
              Expanded(
                flex: 2,
                child: SideMenu(),
              ),
              Expanded(
                flex: 3,
                child: ChatUsers(),
              ),
              Expanded(
                flex: 5,
                child: ChatInfo(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

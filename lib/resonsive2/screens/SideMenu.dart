import 'package:flutter/material.dart';
import 'package:flutter_ui/resonsive2/screens/MyDeviceType.dart';

class SideMenu extends StatefulWidget {
  SideMenu({Key key}) : super(key: key);

  @override
  _SideMenuState createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          // profile
          Expanded(
            flex: 3,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      Spacer(),
                      // close button is only for mobile and tab
                      if (!MyDeviceType.isDesktop(context)) CloseButton(),
                    ],
                  ),
                  CircleAvatar(
                    // backgroundImage: NetworkImage(
                    // 'https://resizing.flixster.com/yk1wTyCznLjJIKhs_4taLnbREk4=/506x652/v2/https://flxt.tmsimg.com/v9/AllPhotos/726719/726719_v9_bc.jpg'),
                    radius: 60,
                  ),
                  Text(
                    'Arun Kenjila',
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      fontFamily: 'sans-serif',
                    ),
                  ),
                ],
              ),
            ),
          ),
          // options(icon + title)
          Expanded(
            flex: 7,
            child: Container(
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.inbox_outlined),
                    title: Text('Inbox'),
                  ),
                  ListTile(
                    leading: Icon(Icons.send_outlined),
                    title: Text('Sent'),
                  ),
                  ListTile(
                    leading: Icon(Icons.drafts_outlined),
                    title: Text('Draft'),
                  ),
                  ListTile(
                    leading: Icon(Icons.add_alert_outlined),
                    title: Text('Spam'),
                  ),
                  ListTile(
                    leading: Icon(Icons.delete_outline_outlined),
                    title: Text('Bin'),
                  ),
                  ListTile(
                    leading: Icon(Icons.settings_outlined),
                    title: Text('Settings'),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

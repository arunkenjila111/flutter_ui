import 'package:flutter/material.dart';
import 'package:flutter_ui/resonsive2/screens/MyDeviceType.dart';

class ChatInfo extends StatefulWidget {
  ChatInfo({Key key}) : super(key: key);

  @override
  _ChatInfoState createState() => _ChatInfoState();
}

class _ChatInfoState extends State<ChatInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          // user info
          Expanded(
              flex: 1,
              child: Center(
                child: Row(
                  children: [
                    if (MyDeviceType.isMobile(context)) BackButton(),
                    Expanded(
                      child: ListTile(
                        leading: CircleAvatar(
                          // backgroundImage: NetworkImage(
                          // 'https://res.cloudinary.com/people-matters/image/upload/q_auto,f_auto/v1561096160/1561096159.jpg'),
                          radius: 30,
                        ),
                        title: Text('Virat'),
                      ),
                    ),
                  ],
                ),
              )),
          //  chat messages
          Expanded(
            flex: 8,
            child: Container(
              color: Colors.white,
              child: Center(
                child: Text('Chat goes here'),
              ),
            ),
          ),
          // sending option
          Expanded(
            flex: 1,
            child: Row(
              children: [
                Expanded(
                  flex: 9,
                  child: Container(
                    padding: EdgeInsets.only(left: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.grey.withOpacity(0.3),
                    ),
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: 'Type Something...',
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: IconButton(
                    icon: Icon(Icons.send_outlined),
                    onPressed: () {},
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_ui/resonsive2/screens/ChatInfo.dart';
import 'package:flutter_ui/resonsive2/screens/MyDeviceType.dart';
import 'package:flutter_ui/resonsive2/screens/SideMenu.dart';

class ChatUsers extends StatefulWidget {
  ChatUsers({Key key}) : super(key: key);

  @override
  _ChatUsersState createState() => _ChatUsersState();
}

class _ChatUsersState extends State<ChatUsers> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        key: scaffoldKey,
        drawer: ConstrainedBox(
          child: SideMenu(),
          constraints: BoxConstraints(maxWidth: 250),
        ),
        body: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Column(
            children: [
              // search users
              Expanded(
                flex: 2,
                child: Column(
                  children: [
                    SizedBox(
                      height: 25,
                    ),
                    Row(
                      children: [
                        //menu button is only for mobile and tablet mode
                        if (!MyDeviceType.isDesktop(context))
                          IconButton(
                            icon: Icon(Icons.menu_outlined),
                            onPressed: () {
                              scaffoldKey.currentState.openDrawer();
                            },
                          ),

                        SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          child: TextField(
                            decoration: InputDecoration(
                              hintText: 'Search',
                              fillColor: Colors.grey.withOpacity(0.2),
                              filled: true,
                              suffixIcon: Icon(Icons.search_outlined),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide: BorderSide.none,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),

              // users list with card
              Expanded(
                flex: 10,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SingeUser(
                        username: "Lucas",
                        lastMessage: "Whats Up",
                        profileUrl:
                            "https://lh3.googleusercontent.com/proxy/vQggFBXNUCniSW4ZgiJGSzqRVfvp10Z_umqa6eOVDYlnmpEAm4riVJ5RjENL2m_L607X31vnsbDkSt5ckZK3YnpYrfQw4MX3FGM9gYmnmGxetFYnSVA4tq_Yt8gsukguWLVXygvH1BGe3Tdv3VLpPdoxWLmSMhHfEJRBmfPbwb6bqVQm21jiaPNccWrKK6y4rdLgDIhil2VaSHEH0IhrXMUkDw1bBHeKb3w4TX8",
                      ),
                      SingeUser(
                          username: "Thor",
                          lastMessage: "Not coming today??",
                          profileUrl:
                              "https://lh3.googleusercontent.com/proxy/aTe36Qwt3Ip9deTuNhPT3MM-F1aFI9VQS2AcY8d6X4Tz2DmSlr2gu79LUY5urL2BkcxySEQfb3limyro1Y_D7LnlGl1SIbC39IY-Gmq_2ue6gpq6BdjEqoST_VDz9ZBZ9cTe9lDBKfzlcmQ1KQLfTovp"),
                      SingeUser(
                        username: "Kenjila",
                        lastMessage: "So... I got it..",
                        profileUrl:
                            "https://qph.fs.quoracdn.net/main-thumb-379185389-200-qljmsoalhkolhbedktstmgjpftsjpczv.jpeg",
                      ),
                      SingeUser(
                        username: "Virat",
                        lastMessage: "In match ro",
                        profileUrl:
                            "https://res.cloudinary.com/people-matters/image/upload/q_auto,f_auto/v1561096160/1561096159.jpg",
                      ),
                      SingeUser(
                        username: "Lucas",
                        lastMessage: "Whats Up",
                        profileUrl:
                            "https://lh3.googleusercontent.com/proxy/vQggFBXNUCniSW4ZgiJGSzqRVfvp10Z_umqa6eOVDYlnmpEAm4riVJ5RjENL2m_L607X31vnsbDkSt5ckZK3YnpYrfQw4MX3FGM9gYmnmGxetFYnSVA4tq_Yt8gsukguWLVXygvH1BGe3Tdv3VLpPdoxWLmSMhHfEJRBmfPbwb6bqVQm21jiaPNccWrKK6y4rdLgDIhil2VaSHEH0IhrXMUkDw1bBHeKb3w4TX8",
                      ),
                      SingeUser(
                          username: "Thor",
                          lastMessage: "Not coming today??",
                          profileUrl:
                              "https://lh3.googleusercontent.com/proxy/aTe36Qwt3Ip9deTuNhPT3MM-F1aFI9VQS2AcY8d6X4Tz2DmSlr2gu79LUY5urL2BkcxySEQfb3limyro1Y_D7LnlGl1SIbC39IY-Gmq_2ue6gpq6BdjEqoST_VDz9ZBZ9cTe9lDBKfzlcmQ1KQLfTovp"),
                      SingeUser(
                        username: "Kenjila",
                        lastMessage: "So... I got it..",
                        profileUrl:
                            "https://qph.fs.quoracdn.net/main-thumb-379185389-200-qljmsoalhkolhbedktstmgjpftsjpczv.jpeg",
                      ),
                      SingeUser(
                        username: "Virat",
                        lastMessage: "In match ro",
                        profileUrl:
                            "https://res.cloudinary.com/people-matters/image/upload/q_auto,f_auto/v1561096160/1561096159.jpg",
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SingeUser extends StatelessWidget {
  final String username;
  final String lastMessage;
  final String profileUrl;

  const SingeUser({
    Key key,
    this.username,
    this.lastMessage,
    this.profileUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: CircleAvatar(
        // backgroundImage: NetworkImage(profileUrl),
        radius: 30,
      ),
      title: Text(username,
          style: TextStyle(
            fontWeight: FontWeight.w700,
          )),
      subtitle: Text(lastMessage),
      onTap: () {
        if (MyDeviceType.isMobile(context))
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => ChatInfo()));
      },
    );
  }
}
